/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.web.handler;

import java.util.List;
import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;


/**
 *
 * @author abraham
 */
public class ErrorHandler extends BodyTagSupport {
    
    /** property declaration for tag attribute: separator.
     *
     */
    private String errorName;
    
    private List errorlist;
    
    private String option;

    public void setOption(String option) {
        this.option = option;
    }
    
    public ErrorHandler() {
        super();
    }

    public void setError(String name){
        errorName=name;
    }
    
    public void setErrorList(List list){
        errorlist = list;
    }
            
 
   /**
     *
     * Generate the links based on the separator attribute passed
     * in to the tag.
     */
    public void otherDoStartTagOperations()  {
        
        
    }
    
    /**
     *
     * Fill in this method to determine if the tag body should be evaluated
     * Called from doStartTag().
     *
     * Return false, since this tag is declared to have an empty body.
     */
    public boolean theBodyShouldBeEvaluated()  {
        if (option!= null && option.equals("not")) return !errorlist.contains(errorName);
        return errorlist.contains(errorName);
    }
    
    /**
     *
     * doEndTag() does nothing
     *
     */
    public void otherDoEndTagOperations()  {
        return;
    }
    
    /**
     *
     * The rest of the JSP page should be evaluated after this tag
     * is finished..
     *
     */
    public boolean shouldEvaluateRestOfPageAfterEndTag()  {
        return true;
    }
    
       
    /** .
     *
     * This method is called when the JSP engine encounters the start tag,
     * after the attributes are processed.
     * Scripting variables (if any) have their values set here.
     * @return EVAL_BODY_INCLUDE if the JSP engine should evaluate the tag body, otherwise return SKIP_BODY.
     * This method is automatically generated. Do not modify this method.
     * Instead, modify the methods that this method calls.
     *
     *
     */
    public int doStartTag() throws JspException, JspException {
        otherDoStartTagOperations();
        
        if (theBodyShouldBeEvaluated()) {
            return EVAL_BODY_BUFFERED;
        } else {
            return SKIP_BODY;
        }
    }
    
    /** .
     *
     *
     * This method is called after the JSP engine finished processing the tag.
     * @return EVAL_PAGE if the JSP engine should continue evaluating the JSP page, otherwise return SKIP_PAGE.
     * This method is automatically generated. Do not modify this method.
     * Instead, modify the methods that this method calls.
     *
     *
     */
    public int doEndTag() throws JspException, JspException {
        otherDoEndTagOperations();
        
        if (shouldEvaluateRestOfPageAfterEndTag()) {
            return EVAL_PAGE;
        } else {
            return SKIP_PAGE;
        }
    }
    
       
    /** .
     * Fill in this method to process the body content of the tag.
     * You only need to do this if the tag's BodyContent property
     * is set to "JSP" or "tagdependent."
     * If the tag's bodyContent is set to "empty," then this method
     * will not be called.
     *
     *
     */
    public void writeTagBodyContent(JspWriter out, BodyContent bodyContent) throws IOException {
   
        
        //
        // write the body content (after processing by the JSP engine) on the output Writer
        //
        bodyContent.writeOut(out);
        
        //
        // Or else get the body content as a string and process it, e.g.:
        //     String bodyStr = bodyContent.getString();
        //     String result = yourProcessingMethod(bodyStr);
        //     out.println(result);
        //
 
        // clear the body content for the next time through.
        bodyContent.clearBody();
    }
    
    /** .
     *
     * Handles exception from processing the body content.
     *
     *
     */
    public void handleBodyContentException(Exception ex) throws JspException {
        // Since the doAfterBody method is guarded, place exception handing code here.
        throw new JspException("error in LinksHandler: " + ex);
    }
    
    /** .
     *
     *
     * This method is called after the JSP engine processes the body content of the tag.
     * @return EVAL_BODY_AGAIN if the JSP engine should evaluate the tag body again, otherwise return SKIP_BODY.
     * This method is automatically generated. Do not modify this method.
     * Instead, modify the methods that this method calls.
     *
     *
     */
    public int doAfterBody() throws JspException {
        try {
            //
            // This code is generated for tags whose bodyContent is "tagdependent"
            //
            
            JspWriter out = getPreviousOut();
            BodyContent bodyContent = getBodyContent();
            
            writeTagBodyContent(out, bodyContent);
        } catch (Exception ex) {
            handleBodyContentException(ex);
        }
        
        if (theBodyShouldBeEvaluatedAgain()) {
            return EVAL_BODY_AGAIN;
        } else {
            return SKIP_BODY;
        }
    }
    
    /**
     * Fill in this method to determine if the tag body should be evaluated
     * again after evaluating the body.
     * Use this method to create an iterating tag.
     * Called from doAfterBody().
     *
     *
     */
    public boolean theBodyShouldBeEvaluatedAgain() {
       
        return false;
    }
   }