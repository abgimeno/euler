/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.web.handler;

import com.euler.web.WizardStep;
import com.euler.web.exception.HandlerException;
import java.util.Hashtable;
import java.util.Vector;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author gerrit
 */
public abstract class WizardHandler extends BaseHandler{

    public static final String PARAMETER_STEP = "step";
    
    public static final String ATTRIBUTE_STEPLIST = "steplist";
    
    /** hash of pagename ("1","2","2b",....) / JSP to show */
    private Hashtable<Integer,WizardStep> steps = new Hashtable();
    
    protected WizardStep actStep;
    protected int nextStep=1;
    protected String page;
    
    
    protected void registerStep(int number, String jsp, String title, String description){
        WizardStep step = new WizardStep();
        step.setNumber(number);
        step.setJsp(jsp);
        step.setTitle(title);
        step.setDescription(description);
        steps.put(number,step);
    }
    
    @Override
    protected boolean validateRequest(HttpServletRequest request, HttpServletResponse response) throws HandlerException {
        String wstep = request.getParameter(PARAMETER_STEP);
        if (wstep!=null){
            // check if it exists
            int n = Integer.parseInt(wstep);
            if (steps.containsKey(n)){
                actStep=steps.get(n);
            }
        }
        else actStep=steps.get(1);
        nextStep=actStep.getNumber();
        
        boolean ret =  validateStep(request,response,actStep);
        request.setAttribute(ATTRIBUTE_STEPLIST, getStepList());
        return ret;
    }

    @Override
    protected void handleRequest(HttpServletRequest request, HttpServletResponse response) throws HandlerException {
        handleStep(request,response,actStep);
        request.setAttribute(ATTRIBUTE_STEPLIST, getStepList());

    }
    
    protected abstract boolean validateStep(HttpServletRequest request, HttpServletResponse response, WizardStep step) throws HandlerException;
    
    protected abstract void handleStep(HttpServletRequest request, HttpServletResponse response, WizardStep step) throws HandlerException;
    
    @Override
    public String getJSP() {
        return page;
    }
    
    protected Vector<WizardStep> getStepList(){
        
        
        int highest=1;
        Vector<WizardStep> list = new Vector();
        for (WizardStep step : steps.values()){
            if (nextStep==step.getNumber()){
                step.setStyleClass("current");
                request.setAttribute("current", step.getNumber());
            } else {
                step.setStyleClass("");
            }
            try {
                list.add(0,step);
                if (step.getNumber()>highest) highest=step.getNumber();
                
                if (step.getNumber() < nextStep){
                    step.setStyleClass("done");
                }
                
                if (step.getNumber() == nextStep-1) {
                    step.setStyleClass("lastDone");
                }
            }
            catch (Exception e){
                //no main step
            }
        }
        Logger.getLogger("com.redamm").info("WizardHandler::getStepList() LAST LOGG act="+actStep.getNumber()); 
        if (highest!=nextStep) steps.get(highest).setStyleClass("mainNavNoBg");
        Logger.getLogger("com.redamm").info("WizardHandler::getStepList() now we show page of step="+nextStep);       

        return list;
    }

}
