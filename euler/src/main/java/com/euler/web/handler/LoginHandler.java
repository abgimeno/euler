package com.euler.web.handler;


import com.euler.info.User;
import com.euler.jpa.controllers.UserJpaController;
import com.euler.web.SessionInfo;


import com.euler.web.forms.LoginForm;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.NoResultException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*
 *
 * @author abraham
 */
public class LoginHandler extends BaseHandler {

    public static final String HANDLERNAME = "login";

    public static final String PAGE_LOGIN = "/WEB-INF/docs/login.jsp";
    public static final String PAGE_MYACCOUNT="/WEB-INF/docs/myaccount.jsp";

    public static final String PAGE_LOGOUT = "index.jsp";


    // Actions for this handler
    public static final String ACTION_LOGIN = "login";
    public static final String ACTION_LOGOUT = "logout";

    protected boolean isAdmin;
    /** Form handling the login-data */
    protected LoginForm loginForm;
    protected UserJpaController userCtl;
    
    private String userType="";

    private static final int adminId=3;


    @Override
    protected boolean validateRequest(HttpServletRequest request, HttpServletResponse response) {
        try {
            
            if(request.getParameter("reg") != null && request.getParameter("reg").equals("true")){
                errorlist.add("register_completed");
            }
            if (action == null) {

            } else {
                if (action.equals(ACTION_LOGIN)) {
                    loginForm = new LoginForm();
                    loginForm.map(request, response);
                    if (isPost) {
                        loginForm.validate(errorlist);
                    }
                    request.setAttribute("loginForm", loginForm);
                } else if (action.equals(ACTION_LOGOUT)) {
                    request.getSession().setAttribute(ATTRIBUTE_USER,null);
                    isUserInSession = false;
                    page = PAGE_LOGOUT;
                }
                
            }
            page=PAGE_LOGIN;
            return errorlist.size() == 0;
        } catch (Exception ex) {
            Logger.getLogger(LoginHandler.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    @Override
    protected void handleRequest(HttpServletRequest request, HttpServletResponse response) {
        try {
      
            loginUser(request,response);


        } catch (NullPointerException ex) {
            Logger.getLogger(LoginHandler.class.getName()).log(Level.SEVERE, "LoginHandler()::handleRequest", ex);
        }
    }



    private void addSessionListenerData(HttpServletRequest request, HttpServletResponse response, User user) {

        ServletContext ctx = request.getSession().getServletContext();
        // find the session info for the actual session
        List<SessionInfo> list = (List<SessionInfo>) ctx.getAttribute("sessionList");
        for (SessionInfo info : list) {
            if (info.getSessionId().equals(request.getSession().getId())) {
                info.setEmail(user.getEmailAccount());
            }
        }
        ctx.setAttribute("sessionList", list);
    }


    private void loginUser(HttpServletRequest request, HttpServletResponse response) {
        try {
            User user = new User();//FrontController.userCtl.login(loginForm.getLogin(), loginForm.getPassword());
            // check if the user is enabled
            if (user.isEnabled()) {
                request.getSession().setAttribute(ATTRIBUTE_USER, user);
                //Adding the information for the SessionListener
                //addSessionListenerData(request, response, user);
                Logger.getLogger(LoginHandler.class.getName()).info("account success, showing index.jsp");
                //check if there are articles in the cart for redirect to the shipping page
                request.getSession().setAttribute("userType", "candidate");
                //request.setAttribute("main_page", PAGE_MAINLOGIN);
                page = PAGE_MYACCOUNT;

            } else {
                errorlist.add("enable_account");
                Logger.getLogger(LoginHandler.class.getName()).info("account disabled, showing disabled.jsp");
                Long userId = user.getId();
                request.setAttribute("uid", userId);
                page = PAGE_LOGIN;
            }

        } catch (NoResultException ex) {
            errorlist.add("login_failed");
            page = PAGE_LOGIN;
        }
    }

    @Override
    protected String getDescription() {
        return description;
    }

    @Override
    protected String getTitle() {
        return "";
    }

    @Override
    public String getJSP() {
        return page;
    }

}
