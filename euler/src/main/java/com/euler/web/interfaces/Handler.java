/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.web.interfaces;

import com.euler.web.exception.HandlerException;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author root
 */
public interface Handler {

    public ArrayList<String> getErrorList();

    /** if you want that a different page is shown then the requested, return here the pagename
     *  start with / !
     * @return
     */
    public String getJSP();

    public boolean isSubmit(String name);

    /** handle the request, get all parameters */
    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws HandlerException;
    
}
