/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.web.form.condition;

import com.euler.web.form.condition.Condition;

/**
 *
 * @author gerrit
 */
public class ConditionCCNumber extends Condition {

    @Override
    public boolean test(String value) throws ValueIsNullException {
        super.test(value);
        try {
            int r = 0;
            int sum = 0;
            for (int x = value.length() - 1; x >= 0; x--) {

                int number = Integer.parseInt("" + value.charAt(x));
                if (r % 2 == 1) {
                    String doubled = "" + (number * 2);
                    sum += Integer.parseInt("" + doubled.charAt(0));
                    if (doubled.length() > 1) {
                        sum += Integer.parseInt("" + doubled.charAt(1));
                    }
                } else {
                    sum += number;
                }
                r++;

            }
            return sum % 10 == 0;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @Override
    public String getName() {
        return "ccnumber";
    }
}
