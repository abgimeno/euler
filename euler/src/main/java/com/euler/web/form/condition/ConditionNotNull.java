/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.web.form.condition;

import java.io.Serializable;

/**
 *
 * @author abraham
 */
public class ConditionNotNull extends Condition  implements Serializable{
    
    @Override
    public boolean test(String value){
        return value!=null && !value.equals("");
    }

    @Override
    public String getName() {
        return "notnull";
    }
    
    
}
