/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.web.form.condition;

import java.io.Serializable;

/**
 *
 * @author abraham
 */
public class ConditionLength extends Condition  implements Serializable{

    private int min;
    private int max;

    public ConditionLength(int min, int max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public boolean test(String value) throws ValueIsNullException {
        
        super.test(value);          
        if ((min != -1 && value.length() < min) || (max != -1 && value.length() > max)) {
            return false;
        }
        return true;
    }

    @Override
    public String getName() {
        return "length";
    }
}
