/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.web.form.condition;

import com.sun.org.apache.regexp.internal.RE;
import com.sun.org.apache.regexp.internal.RESyntaxException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author abraham
 */
public class ConditionRegExp extends Condition {

    private Pattern pattern;
    private Matcher matcher;

    
    public ConditionRegExp(String expression) throws RESyntaxException {
       pattern = Pattern.compile(expression);
    }

    @Override
    public boolean test(String value) throws ValueIsNullException {
        super.test(value);
        matcher = pattern.matcher(value);
        return matcher.matches();
    }

    @Override
    public String getName() {
        return "regexp";
    }
    
}
