/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.web.form.condition;

/**
 * @author abraham
 */
public class Condition {
  
    public static final String NULL_MSG =  "The value to be tested in the condition is NULL and hence cannot be tested";

    public boolean test(String value) throws ValueIsNullException {
        nullValueTest(value);
        return true;
    }
    
    public void nullValueTest(String value) throws ValueIsNullException {
        if (value == null) {
            throw new ValueIsNullException(NULL_MSG);
        }        
    }
    
    public String getName() {
        return "base condition class";
    }
}
