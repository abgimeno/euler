/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.web.form.condition;

import com.sun.org.apache.regexp.internal.RESyntaxException;
import java.text.NumberFormat;

/**
 *
 * @author abraham
 */
public class ConditionIsInteger extends Condition {

    //public static final Integer nf = Integer.parseInt();

    
    public ConditionIsInteger() throws RESyntaxException {
    }

    @Override
    public boolean test(String value) {
        try {
            Integer d = Integer.parseInt(value);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    @Override
    public String getName() {
        return "isInteger";
    }
}
