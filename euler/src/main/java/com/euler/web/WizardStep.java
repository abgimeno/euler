/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.web;

/** data container for all data we need for a step in a wizard
 *
 * @author gerrit
 */
public class WizardStep {
    
    private String title;
    
    private String description;
    
    private String jsp;
    
    private int number;

    private String styleClass;
    
    private int activated;
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getJsp() {
        return jsp;
    }

    public void setJsp(String jsp) {
        this.jsp = jsp;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStyleClass() {
        return styleClass;
    }

    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }

    public int getActivated() {
        return activated;
    }

    public void setActivated(int activated) {
        this.activated = activated;
    }
    
    

}
