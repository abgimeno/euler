/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.web.exception;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abraham
 */
public class RedirectException extends RuntimeException{
    
    public String url;
    
    public RedirectException (String url){
      try{  
        this.url= url;
        }catch(NullPointerException excNull){
            Logger.getLogger(RedirectException.class.getName()).log(Level.SEVERE, "RedirectException::RedirectException()");
        }
    }

    public String getUrl() {
      try{  
        return url;
       }catch(NullPointerException excNull){
            Logger.getLogger(RedirectException.class.getName()).log(Level.SEVERE, "RedirectException::RedirectException()");
        }
        return null;
    }

    public void setUrl(String url) {
     try{   
        this.url = url;
        }catch(NullPointerException excNull){
            Logger.getLogger(RedirectException.class.getName()).log(Level.SEVERE, "RedirectException::RedirectException()");
        }        
    }

    
}
