/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.web.paging;

import java.util.Iterator;
import java.util.List;


/**
 * Containerclass to put all info about paging. Class is used in the
 * taglib web.PagingHandler.
 * 
 * check taglib web for more info.
 * 
 * @author gerrit
 */
public class PageInfo {

    public static final String PARAMETER_PAGE="page";
    
    /** actual page, counting from 0 to pages-1 */
    private int actPage;
    
    /** number of pages */
    private int pages;
    
    /** size of 1 page **/
    private int pageSize;
    
    /** baseurl, including a & or ? at the end for the parameter Page */
    private String baseUrl;
    
    /** if the paging is based on a list, her is the data */
    private List list;
   
    /** Constructor for a new PageInfo instance.
     * 
     * @param baseUrl baseurl to generate the links
     * @param pages number of all pages
     */
    public PageInfo(String baseUrl, int pageSize){
            setBaseUrl(baseUrl);
            setPagesCount(pages);
    }
    
    public PageInfo(List list,String baseUrl, int pageSize){
        this(baseUrl,0);
        this.setPagesCount((list.size()-1)/pageSize+1);
        this.list=list;
        this.pageSize=pageSize;
    }
    
    public Iterator getIterator(){
        return new PagingIterator(list.iterator(),pageSize,actPage);
    }
    
    public int getActPage() {
        return actPage;
    }
    
    public void setActPage(int p){
        actPage=p;
    }
    
    public int getPageCount() {
        return pages;
    }
    
    public void setPagesCount(int count) {
        pages=count;
    }
    
    public void setBaseUrl(String url){
        baseUrl= url;
    }
    
    public String getBaseUrl(){
        return baseUrl;
    }
    
    /** returns a link to the previous page, based on BASEURL */
    public String getPrevLink(){
        if (actPage==0) return null;
        return baseUrl+PARAMETER_PAGE+"="+(actPage-1);
    }
    
    /** returns a link to the next page, based on BASEURL */
    public String getNextLink(){
        if (actPage+1>=pages) return null;
        return baseUrl+PARAMETER_PAGE+"="+(actPage+1);
    }
    
    /** returns a link to the first page, based on BASEURL */
    public String getFirstLink(){
        return baseUrl+PARAMETER_PAGE+"="+0;
    }
    /** returns a link to the next page, based on BASEURL */
    public String getLastLink(){
        return baseUrl+PARAMETER_PAGE+"="+(pages-1);
    }
    
    
}
