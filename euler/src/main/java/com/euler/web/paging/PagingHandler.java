/*
 * TagHandler for the tag web:paging
 * 
 * the Tag <web/paging pageinfo=""> gets a PageInfo object and sets the 4 attributes
 * next,prev,act and pages.
 * 
 * 
 * 
 * @author Gerrit
 */
package com.euler.web.paging;

import java.io.IOException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;


/**
 *
 * @author gerrit
 */
public class PagingHandler extends SimpleTagSupport {

    private PageInfo pageInfo;
    public static final String PARAM_NEXT = "next";
    public static final String PARAM_PREV = "prev";
    public static final String PARAM_ACT = "act";
    public static final String PARAM_PAGES = "pages";

    public static final String NAME_PREV="Anterior";
    public static final String NAME_NEXT="Siguente";
    public static final String NAME_FIRST="Anterior";
    public static final String NAME_LAST="Siguente";
    
    public static final String PICTURE_FIRST="home.png";
    public static final String PICTURE_PREV="prev.png";
    public static final String PICTURE_NEXT="next.png";
    public static final String PICTURE_LAST="end.png";
    
    public static final int DISPLAY_PAGES=7;
    
    int displayPages= DISPLAY_PAGES;
    
    boolean moreLeft=false;
    boolean moreRight=false;
    
    String basePicurl="";
    
    private boolean hideSingle=true;

    public boolean isHideSingle() {
        return hideSingle;
    }

    public void setHideSingle(boolean hideSingle) {
        this.hideSingle = hideSingle;
    }
    
    public void setPageInfo(PageInfo info) {
        pageInfo = info;
    }

    @Override
    public void doTag() throws JspException, IOException {
        
        if (pageInfo != null) {
            if (hideSingle && pageInfo.getPageCount()==1) return;
            String prev = pageInfo.getPrevLink();
            String next = pageInfo.getNextLink();
            
            if (next!=null) next = "<a href="+next+">"+NAME_NEXT+"</a>";
            else next="";
            if (prev!=null) prev = "<a href="+prev+">"+NAME_PREV+"</a>";
            else prev="";
            getJspContext().setAttribute(PARAM_NEXT, next );
            getJspContext().setAttribute(PARAM_PREV, prev);
            getJspContext().setAttribute(PARAM_ACT, (pageInfo.getActPage() + 1));
            //getJspContext().setAttribute(PARAM_PAGES, pageInfo.getPageCount());
            
            String s="";
            String num="";
            for (Integer p: getDisplayPages()) {
                if (pageInfo.getActPage()==p) num+="<span>"+(p+1)+"</span> ";
                else num+=createLink(pageInfo.getBaseUrl(),p);
            }
            s+="<div class=\"num\">"+num+"</div>";
            if (moreLeft)s="<div class=\"dotsleft\">...</div>"+s;
            if (moreRight)s+="<div class=\"dotsright\">...</div>";
            s= createImageLink(PICTURE_FIRST,pageInfo.getFirstLink(),pageInfo.getActPage()!=0 )+
               createImageLink(PICTURE_PREV,pageInfo.getPrevLink(),pageInfo.getActPage()!=0 )+s;
            s+= createImageLink(PICTURE_NEXT, pageInfo.getNextLink(),pageInfo.getActPage()<pageInfo.getPageCount()-1 );
            s+= createImageLink(PICTURE_LAST, pageInfo.getLastLink(),pageInfo.getActPage()<pageInfo.getPageCount()-1);
            
            
            
            getJspContext().setAttribute(PARAM_PAGES, s);
            
            getJspBody().invoke(null);
        }
        else
            {
            Logger.getLogger(PagingHandler.class.getName()).log(Level.INFO,"PagingHandler::doTag() no pageinfo");    
        }
    }
    
    protected String createImageLink(String image, String url, boolean active){
        if (active) return "<a href=\""+url+"\"><img src=\""+basePicurl+image+"\"></a>";
        return "<img src=\""+basePicurl+image+"\">";
    }
    
    protected String createLink(String url, int page){
        return "<a href=\""+url+"page="+page+"\">"+(page+1)+"</a>";
    }
    
    protected Vector<Integer> getDisplayPages(){
        Vector<Integer> pages = new Vector();
        
        int act= pageInfo.getActPage();
        int left = pageInfo.getActPage();
        if ( left > displayPages/2) left=displayPages/2;
        
        int right= displayPages-1-left;
        if (right > displayPages/2) right=displayPages/2;
        
        
        if (left < displayPages/2) right+= displayPages/2-left;
        
            
        if (right+act> pageInfo.getPageCount() ) right= pageInfo.getPageCount()-act-1;
        if (right < displayPages/2) left+= displayPages/2-right;
        
        if (act-left<0) left=act;
        
        //moreleft and right
        if (act>left) moreLeft=true;
        if (act+right+1<pageInfo.getPageCount()) moreRight=true;
        
        
        System.out.println("left:"+left+" right:"+right+" act:"+pageInfo.getActPage()+" mr="+moreRight);
        
        while (left>0){
            pages.addElement(act-left);
            left --;
        }
        
        pages.add(act);
        
        while (right-->0) pages.addElement(++act);
        return pages;
    }

    public void setBasePicurl(String basePicurl) {
        this.basePicurl = basePicurl;
    }

}
