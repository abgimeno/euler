/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.web.forms;


import com.euler.web.form.condition.ConditionEmail;
import com.euler.web.form.condition.ConditionLength;
import com.euler.web.form.condition.ConditionNotNull;
import com.sun.org.apache.regexp.internal.RESyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abry
 */
public class LoginForm extends Form {

    public static final String PARAM_USERLOGIN = "userlogin";
    public static final String PARAM_PASSWORD = "password";
    private Parameter userlogin;
    private Parameter password;
    /**
     * 
     */
    public LoginForm() {

        try {
            userlogin = new Parameter(PARAM_USERLOGIN, Parameter.ptype.STRING);
            userlogin.addCondition(new ConditionNotNull());
            userlogin.addCondition(new ConditionLength(0, 64));
            userlogin.addCondition(new ConditionEmail());

            addParameter(userlogin);
            password = new Parameter(PARAM_PASSWORD, Parameter.ptype.STRING);
            password.addCondition(new ConditionNotNull());
            password.addCondition(new ConditionLength(5, 20));
            addParameter(password);
        } catch (RESyntaxException ex) {
            Logger.getLogger(LoginForm.class.getName()).log(Level.SEVERE,"LoginForm()Constructor", ex);
        }
    }

    public String getLogin() {
        return this.userlogin.getStringValue();
    }

    public String getPassword() {
        return this.password.getStringValue();
    }
}


