

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.web.taglib;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author abraham
 */
public class Analytics extends SimpleTagSupport {
    
    private String uacct;
    
    public void setUacct(String s){
        uacct=s;
    }
    
    @Override 
    public void doTag() throws JspException, IOException {
        JspWriter out = this.getJspContext().getOut();
        
        out.write("<script type=\"text/javascript\">");
        out.write("var pageTracker = _gat._getTracker(\"UA-2034011-4\");");
        out.write("pageTracker._initData();");
        out.write("pageTracker._trackPageview();");
        out.write("</script>");
    }
}

