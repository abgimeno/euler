/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.web.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author abraham
 */
public class NumberSelect extends SimpleTagSupport {

    private int from;
    private int to;
    private String name;
    private String id;
    private String htmlClass;
    private int value = 1;

    @Override
    public void doTag() throws JspException, IOException {
        JspWriter out = this.getJspContext().getOut();

        out.write("<select name=\"" + name + "\" ");
        if (id != null) {
            out.write("id=\"" + id + "\"");
        }
        if (htmlClass != null) {
            out.write("class=\"" + htmlClass + "\" ");
        }
        out.write(">");
       
        for (int ctr = from; ctr <= to; ctr++) {
             String s = "";
            if (ctr == value) {
                s = " selected";
            }
            out.write("<option value=\"" + ctr + "\"" + s + ">" + ctr + "</option>");
        }
        out.write("</select>");
    }

    public String getHtmlClass() {
        return htmlClass;
    }

    public void setHtmlClass(String htmlClass) {
        this.htmlClass = htmlClass;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
