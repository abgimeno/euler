/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.web.taglib;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *SPAIN                                           ES      ESP     724

 * @author abraham
 */
public class DaySelect extends SimpleTagSupport {
    
    
    SimpleDateFormat dayFormat = new SimpleDateFormat("dd",new Locale("es"));
    
    private String name;
    
    private String id;

    private String htmlClass;
    
    private int value=1;
    
    @Override
    public void doTag() throws JspException, IOException {
        JspWriter out = this.getJspContext().getOut();

        GregorianCalendar d = new GregorianCalendar();
        Integer year = d.get(GregorianCalendar.YEAR);
        getJspContext().setAttribute("currentyear", year);
        getJspContext().setAttribute("startyear", year - 50);
        
        out.write("<select name=\""+name+"\" ");
        if (id!=null) out.write("id=\""+id+"\" ");
        if (htmlClass!=null) out.write("class=\""+htmlClass+"\" ");
        out.write(">");
        GregorianCalendar gc = new GregorianCalendar();
        
        for (int ctr=1;ctr<=31;ctr++){
            String s = "";
            gc.set(2008, 0, ctr);
            if (ctr==value) s = " selected";
            out.write("<option value=\""+ctr+"\""+s+">"+dayFormat.format(gc.getTime())+"</option>");
        }
        out.write("</select>");
    }

    public String getHtmlClass() {
        return htmlClass;
    }

    public void setHtmlClass(String htmlClass) {
        this.htmlClass = htmlClass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setValue(int value) {
        this.value = value;
    }
    
    
}
