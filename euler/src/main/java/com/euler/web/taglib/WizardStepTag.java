/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.web.taglib;

import com.euler.web.WizardStep;
import java.io.IOException;
import java.util.List;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author gerrit
 */
public class WizardStepTag extends SimpleTagSupport {

    private String handler;
    protected List<WizardStep> steps;
    private int currentStep;

    public void setHandler(String s) {
        this.handler = s;
    }

    public void setCurrentStep(Integer s) {
        this.currentStep = s;
    }

    public void setSteps(List<WizardStep> steps) {
        this.steps = steps;
    }

    @Override
    public void doTag() throws JspException, IOException {
        JspWriter out = this.getJspContext().getOut();


        out.write("<ul id=\"mainNav\" class=\"fiveStep\">");
        for (WizardStep step : steps) {
            out.write("<li class=\"" + step.getStyleClass() + "\">");

            if ( step.getNumber() != 1) {
             // REMOVED STEP NUMBER   out.write("<em>Step " + step.getNumber() + ":</em><span>" + step.getDescription() + "</span>");
                out.write("<span>" + step.getDescription() + "</span>");
            } else {
             // REMOVED STEP NUMBER  out.write("<a href=\"" + handler + "&step=" + step.getNumber() + "\" title=\"\"><em>Step " + step.getNumber() + ":</em><span>" + step.getDescription() + "</span></a>");
                  out.write("<a href=\"" + handler + "&step=" + step.getNumber()+"&rp=1" + "\" title=\"\"><span>" + step.getDescription() + "</span></a>");
            }

            out.write("</li>");
        }
        //out.write("</web:forEach>");
        out.write("</ul>");
    }
}

