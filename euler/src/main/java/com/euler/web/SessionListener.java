/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.web;

import java.util.List;
import java.util.Vector;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 *
 * @author gerrit
 */
public class SessionListener implements HttpSessionListener {

    public void sessionCreated(HttpSessionEvent event) {
        System.out.println("SESSION CREATE");
        HttpSession session = event.getSession();
        ServletContext ctx = session.getServletContext();
        SessionInfo sessionInfo = new SessionInfo(event.getSession().getId(), 
                                                    event.getSession().getCreationTime(),
                                                    (String) event.getSession().getAttribute("email"),
                                                    (String) event.getSession().getAttribute("domain"));
        List<HttpSession> sessionList = (List) ctx.getAttribute("sessionList");
        if (sessionList == null) {
            sessionList = new Vector();
            ctx.setAttribute("sessionList", sessionList);
        }
        sessionList.add(session);
    }

    public void sessionDestroyed(HttpSessionEvent event) {
        System.out.println("SESSION DESTROYED");
        HttpSession session = event.getSession();
        ServletContext ctx = session.getServletContext();
        List<HttpSession> sessionList = (List) ctx.getAttribute("sessionList");
        if (sessionList == null) return;
        sessionList.remove(event.getSession());
    }
}
