/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.io;

import com.euler.web.forms.Form;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

/**
 *
 * @author abraham
 */
public class SaveFile {
 protected Logger logger = Logger.getLogger(Form.class.getName());
 public SaveFile ( ) {
     
 }   
  
/**
 * Store the input stream in the pathDestination 
 * and names the file with the outputFile name
 * @param pathDestination, the path where file will be stored
 * @param outputFile, data for the outputfile will be created
 * @param inputStream
 * @throws java.io.FileNotFoundException
 * @throws java.io.IOException
 */    
    
public void doSave(File pathDestination, File outputFile, InputStream inputStream) throws FileNotFoundException, IOException {
       
        byte b[] = new byte[256];
        int len;
        File out = new File(pathDestination.getPath() + "/" + outputFile.getName());
        logger.info("Saving " + out.getPath() + "  .....");
        FileOutputStream fileOutput = new FileOutputStream(out);
        do {

            len = inputStream.read(b);
            if (len > 0) {
                fileOutput.write(b, 0, len);
            }
        } while (len > 0);
    }

}
