/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
/**
 *
 * @author abraham
 */
public class ZipUtil {

    public static void addFileToZip(ZipOutputStream zout, File file, File root) {
        String filename = file.getName();
        File parent = file.getParentFile();
        while (parent != null && (!parent.equals(root))) {
            filename = parent.getName() + "/" + filename;
            parent = parent.getParentFile();
        }


        Logger.getLogger(ZipUtil.class.getName()).info("ZipUtil::addFileToZip() adding file [" + filename + "]");
        byte b[] = new byte[300000];
        FileInputStream fis;
        int numRead = 0;
        try {
            fis = new FileInputStream(file);
            zout.putNextEntry(new ZipEntry(filename));

            while (numRead != -1) {
                numRead = fis.read(b, 0, 20000);
                if (numRead > 0) {
                    zout.write(b, 0, numRead);
                }
            }

            zout.closeEntry();
        } catch (Exception ex) {
            Logger.getLogger(ZipUtil.class.getName()).log(Level.SEVERE,"ZipUtil::addFileToZip exception", ex);
        }
    }

    public static void addPathToZip(ZipOutputStream zout, File dir, File root) {
        for (File file : dir.listFiles()) {
            String filename = file.getName();
            File parent = file.getParentFile();
            while (parent != null && (!parent.equals(root))) {
                filename = parent.getName() + "/" + filename;
                parent = parent.getParentFile();
            }
            filename+="/";
            if (file.isDirectory()) {
                
                try {
                    Logger.getLogger(ZipUtil.class.getName()).info("ZipUtil::addPathToZip() adding file [" + filename + "]");
                    zout.putNextEntry(new ZipEntry(filename));
                } catch (IOException ex) {
                    Logger.getLogger(ZipUtil.class.getName()).log(Level.SEVERE,"ZipUtil::addPathToZip exception", ex);
                }
                addPathToZip(zout, file, root);
            } else {
                addFileToZip(zout, file, root);
            }
        }
    }

    public static void createZip(File zipfile, File path, File root) {
        ZipOutputStream out = null;
        try {
            out = new ZipOutputStream(new FileOutputStream(zipfile));
            addPathToZip(out, path, root);
            out.flush();
            out.close();
        } catch (Exception ex) {
            Logger.getLogger(ZipUtil.class.getName()).log(Level.SEVERE,"ZipUtil::createZip() execption:", ex);
        } finally {
            try {
                out.close();
            } catch (IOException ex) {
                Logger.getLogger(ZipUtil.class.getName()).log(Level.SEVERE,"ZipUtil::createZip() execption:", ex);
            }
        }
    }

    public static void extractZip(Logger logger, File zipfile, File extractTo) {
        final int BUFFER = 2048;
        try {
            BufferedOutputStream dest = null;
            FileInputStream fis = new FileInputStream(zipfile);
            ZipInputStream zis = new ZipInputStream(new BufferedInputStream(fis));
            ZipEntry entry;
            while ((entry = zis.getNextEntry()) != null) {

                if (entry.isDirectory()) {
                    File newdir = new File(extractTo, entry.getName());
                    logger.info("ZipUtil::extractZip() creatig dir:" + entry.getName());
                    newdir.mkdir();
                } else {
                    logger.info("ZipUtil::Extracting: " + entry);
                    int count;
                    byte data[] = new byte[BUFFER];
                    // write the files to the disk
                    File output = new File(extractTo, entry.getName());
                    FileOutputStream fos = new FileOutputStream(output);
                    dest = new BufferedOutputStream(fos, BUFFER);
                    while ((count = zis.read(data, 0, BUFFER)) != -1) {
                        dest.write(data, 0, count);
                    }
                    dest.flush();
                    dest.close();
                }
            }
            zis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    
    public static void main( String args[]) {
        File zip = new File("/tmp/test-zip/");
        File dir = new File("/Users/abraham/Desktop/onyx");
        ZipUtil.createZip(zip, dir, dir);
        
        
    }


}
