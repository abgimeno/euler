/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.configuration;

import java.util.Properties;

/**
 *
 * Singleton to store configurationdata
 * 
 * 
 * @author abraham
 */
public class Configuration {

    /** Properties class */
    private Properties config;
    /** Singleton instance */
    private static Configuration singleton = null;
    /** name of the paramater for main datat dir */
    public static final String DATAPATH = "DATAPATH";

    protected Configuration(Properties config) {
        setProperties(config);
    }

    public static Configuration initInstance(Properties config) {
        if (singleton == null) {
            singleton = new Configuration(config);
        }
        return singleton;
    }

    public static Configuration getInstance() {
        return singleton;
    }

    public void setProperties(Properties config) {
        this.config = config;
    }

    public static String getProperty(String name) {
        return singleton.config.getProperty(name);
    }

    public static String getDataPath() {
        return singleton.config.getProperty(DATAPATH);
    }

    public Properties getProperties() {
        return config;
    }

    public void setConfig(Properties config) {
        this.config = config;
    }

}
