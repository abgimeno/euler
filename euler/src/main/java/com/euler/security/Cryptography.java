
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author abraham
 */
public class Cryptography {

    private static final String SALT = "Malaka";
    private static final String ALGORITHM = "SHA-256";

    public Cryptography() {
    }

    /***
     * String encription hashing md5 algorithm
     * @param login
     * @param plain
     * @return
     */
    public static String cryptoPassword(String login, String plain) {
        try {
            Base64 base64 = new Base64();

            MessageDigest md = MessageDigest.getInstance("MD5");

            md.reset();
            md.update(login.getBytes());
            md.update(plain.getBytes());
            md.update(SALT.getBytes());

            String pwd = new String(base64.encode(md.digest()));
            return pwd;

        } catch (Exception ex) {
            System.out.println("" + ex);
        }
        return null;
    }

    /**
     * 
     * @param stringForHash
     * @return
     */
    public static String hashWithDate(String hashThisString) {
        try {
            byte[] stringForHash = hashThisString.getBytes();
            Base64 base64 = new Base64();
            MessageDigest digest = MessageDigest.getInstance(ALGORITHM);
            byte[] hashBytes = digest.digest(stringForHash);

            byte dateBytes;
            Date date = new Date();
            dateBytes = (byte) date.hashCode();
            digest.update(dateBytes);
            digest.update(SALT.getBytes());

            String hashString = new String(base64.encode(hashBytes));

            return hashString;
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Cryptography.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @SuppressWarnings("static-access")
    public static void main(String[] args) {
        String s = "stmr234";
        
       // System.out.println((s.replaceFirst("stmr","")));
        System.out.println(cryptoPassword("abraham@eulercomputing.com", "test123"));
    }
}
