/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.beans;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author gerrit
 */
public class BeanManager {    
    
    public static Object getBean(String name){
        try {
            Context context = new InitialContext();
            Object o = context.lookup("java:comp/env/" + name);
         
            return o;
            
        } catch (NamingException ex) {
            Logger.getLogger(BeanManager.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
