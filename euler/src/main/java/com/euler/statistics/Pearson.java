/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 */

package com.euler.statistics;

import java.util.List;


/**
 *
 * @author abraham
 */

public class Pearson extends Similarity {    

    private List<Rating> prefs1;    
    private List<Rating> prefs2;               
        
    private double sum1;
    private double sum2;
    private double sumSq1;
    private double sumSq2;
    private double pSum;
    private int n;
    
    public Pearson(){
        
    }
    /****
     * 
     * prefs1 and prefs2 are the same items rated by different users
     * all the items containing the pref1 must belong to a 
     * unique user and prefs2 to an unique user  different to user1
     * @param prefs1 
     * @param prefs2
     *  
     */
    public Pearson(List<Rating> prefs1, List<Rating> prefs2) {
        this.prefs1 = prefs1;
        this.prefs2 = prefs2;
        n=prefs1.size();
    }
    
    public double calculatePearson(){
        
        for(Rating r : prefs1){
            sum1=+r.getRate();
            sumSq1+= Math.pow(r.getRate(),2);
        }
        for(Rating r : prefs2){
            sum2=+r.getRate();
            sumSq2+=Math.pow(r.getRate(),2);
        }
        for(int i= 0; i<n;i++){
            pSum+= (prefs1.get(i).getRate() * prefs2.get(i).getRate());             
        }    
        double num = pSum - (sum1*sum2/n);
        double den = Math.sqrt((sumSq1- Math.pow(sum1,2)/n) * (sumSq2- Math.pow(sum2,2)/n));
        if (den ==0) 
                return 0;
        return num/den;
        
    }    

    @Override
    public double getSimilarity() {
        return calculatePearson();
    }
}
