/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.reports;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import java.io.OutputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abraham
 * 
 */
public class iTextReport {

    /**
     * 
     * @param template, Takes as an input an AcroForm, 
     * @param fields, list of fields
     * @param values, list of values
     * @return
     */
    public static File createReportFile(File template, List<String> fields, List<String> values) {
        Document document = new Document();
        try {
            if (fields.size() != values.size()) {
                return null;
            }
            File file = new File(template.getAbsolutePath() + "_report.pdf");

            FileOutputStream report = new FileOutputStream(file);
            PdfReader reader = new PdfReader(template.getPath());
            PdfStamper stamper = new PdfStamper(reader, report);
            AcroFields form = stamper.getAcroFields();
            for (int i = 0; i < fields.size(); i++) {
                form.setField(fields.get(i), values.get(i));
            }
            stamper.close();
            document.close();
            return (file);

        } catch (DocumentException ex) {
            Logger.getLogger(iTextReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (IOException ex) {
            Logger.getLogger(iTextReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    /**
     * 
     * @param outputName, file output name
     * @param template, 
     * @param fields
     * @param values
     * @return
     */
    public static File createReportFile(String outputName, File template, List<String> fields, List<String> values) {
        Document document = new Document();
        try {
            if (fields.size() != values.size()) {
                return null;
            }
            File file = new File(template.getAbsolutePath() + outputName + ".pdf");
            FileOutputStream report = new FileOutputStream(file);
            PdfReader reader = new PdfReader(template.getPath());
            PdfStamper stamper = new PdfStamper(reader, report);
            AcroFields form = stamper.getAcroFields();

            for (int i = 0; i < fields.size(); i++) {
                form.setField(fields.get(i), values.get(i));
            }
            stamper.close();
            document.close();
            return (file);

        } catch (DocumentException ex) {
            Logger.getLogger(iTextReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (IOException ex) {
            Logger.getLogger(iTextReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    public static void createReportFile(OutputStream output, File template, List<String> fields, List<String> values) {
        Document document = new Document();
        try {
            if (fields.size() != values.size()) {
                Logger.getLogger(iTextReport.class.getName()).log(Level.SEVERE, "fields!=values");

                return;
            }
            PdfReader reader = new PdfReader(template.getPath());
            PdfStamper stamper = new PdfStamper(reader, output);
            AcroFields form = stamper.getAcroFields();

            for (int i = 0; i < fields.size(); i++) {
                form.setField(fields.get(i), values.get(i));
            }
            stamper.setFormFlattening(true);
            stamper.close();
            document.close();
            return;

        } catch (DocumentException ex) {
            Logger.getLogger(iTextReport.class.getName()).log(Level.SEVERE, null, ex);
            return;
        } catch (IOException ex) {
            Logger.getLogger(iTextReport.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
    }
}
