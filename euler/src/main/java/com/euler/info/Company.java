/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.info;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import com.euler.info.Address;




/**
 *
 * @author abraham
 * 
 */

@Embeddable
public class Company implements Serializable {
    
    @Column(name="COMPANY_NAME")
    private String companyName;
    
    @Embedded 
    Address address;
    
    @Column(name="PHONE_NUMBER")
    private String phoneNumber;
    
    @Column(name="FAX_NUMBER")
    private String faxNumber;
    
    @Column(name="EMAIL")
    private String email;
    
    @Column(name="NIF")
    private String nif;
    
    @Column(name="LEGAL_INFO", length=3000)
    private String legalInfo;
    
    @Column(name="LOCALIZATION", length=3000)
    private String  localization;
    
    @Column(name="CONTACT", length=3000)
    private String contact;
    
    @Column(name="DESCRIPTION", length=3000)
    private String description;
    
    public Company() {
        address = new Address();
    }
    
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLegalInfo() {
        return legalInfo;
    }

    public void setLegalInfo(String legalInfo) {
        this.legalInfo = legalInfo;
    }

    public String getLocalization() {
        return localization;
    }

    public void setLocalization(String localization) {
        this.localization = localization;
    }

}
