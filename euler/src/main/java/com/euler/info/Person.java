package com.euler.info;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Embeddable;

import javax.persistence.Embedded;
import javax.persistence.Temporal;


@Embeddable
public class Person implements Serializable {


  public static final String  nameName = "name";
  public static final String  surname1Name="surname1";
  public static final String  surname2Name="surname2";
  public static final String  emailName="emailName";
  public static final String  publicPhoneName="publicPhoneName";
  public static final String  mobilePhoneName="mobilePhone";
  public static final String  birthdateName="birthDate";

  @Column(name="NAME")
  private String name;

  @Column(name="SURNAME1")
  private String surname1;

  @Column(name="SURNAME2")
  private String surname2;

  @Column(name="DNI")
  private String dni;
  
  @Column(name="EMAIL")
  private String email;
  
  @Column(name="PUBLIC_NUMBER")
  private String publicPhone;
  
  @Column(name="MOBILE_NUM")
  private String mobilePhone;  
  
  @Temporal(javax.persistence.TemporalType.DATE)
  @Column(name="BIRTHDATE")
  private Date birthdate;  
  
  @Embedded
  private Address address;

  public Person () {
      address = new Address();
  }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname1() {
        return surname1;
    }

    public void setSurname1(String surname1) {
        this.surname1 = surname1;
    }

    public String getSurname2() {
        return surname2;
    }

    public void setSurname2(String surname2) {
        this.surname2 = surname2;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPublicPhone() {
        return publicPhone;
    }

    public void setPublicPhone(String publicPhone) {
        this.publicPhone = publicPhone;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }
   
}