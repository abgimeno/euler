package com.euler.testing;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abraham
 */
public class Numbers {

    private static String PARAM_ST = "sorteo";
    private static String VALUE_ST = "vista_102_2008";
    private static String PARAM_NUM = "numero";

    public static void main(String[] args) {

        createFileList("/Users/abraham/mpeg_low.asp", "/Users/abraham/mpeg_low.txt");
        createFileList("/Users/abraham/mpeg_medium.asp", "/Users/abraham/mpeg_medium.txt");
        createFileList("/Users/abraham/mpeg_good.asp", "/Users/abraham/mpeg_good.txt");
        createFileList("/Users/abraham/mpeg_superior.asp", "/Users/abraham/mpeg_superior.txt");
        
    }


    private static void createFileList(String input, String output) {
        FileOutputStream out = null;
        FileInputStream file = null;
        try {            
            out = new FileOutputStream(new File(output));            
            String text = "";
            System.out.println("Iniciando");
            file = new FileInputStream(input);
            BufferedReader rd = new BufferedReader(new InputStreamReader(file));
            String line = "";
            line = "";
            while ((line = rd.readLine()) != null) {
                if (line.contains("openplayer")) {
                    int start = line.indexOf("openplayer");
                    text = line.substring(start + 11, start + 13) + ".asx\n";
                    System.out.println(text);
                    out.write(text.getBytes());
                }
            }
            text = "";
            out.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Numbers.class.getName()).log(Level.SEVERE, null, ex);
        }catch (IOException ex) {
            Logger.getLogger(Numbers.class.getName()).log(Level.SEVERE, null, ex);
        }  finally {
            try {
                out.close();
            } catch (IOException ex) {
                Logger.getLogger(Numbers.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
