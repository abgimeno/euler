/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.testing;

import junit.framework.Test;
import junit.framework.TestFailure;

/**
 *
 * @author gerrit
 */
public class TestWrapper {

    String exceptionMessage="";
    
    String testName;

    public TestWrapper(Test test) {
        testName = test.getClass().getName();
    }

    public void setFailure(TestFailure tf) {
        exceptionMessage = tf.exceptionMessage();
    }

     public String getExceptionMessage() {
        return exceptionMessage;
    }

    public String getException() {
        return exceptionMessage;
    }

    public String getTestName() {
        return testName;
    }

    public String getStatus() {
        return exceptionMessage.equals("") ? "ok" : "fail";
    }
}
