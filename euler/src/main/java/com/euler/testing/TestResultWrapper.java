/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.testing;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;
import junit.framework.Test;
import junit.framework.TestFailure;
import junit.framework.TestResult;
import junit.framework.TestSuite;


/**
 *
 * @author gerrit
 */
public class TestResultWrapper implements Serializable{

    private String name;
    
    private int errorCount;
    
    private int failureCount;
    
    private boolean ok;
          

    
    private Vector<TestWrapper> results = new Vector();

    public TestResultWrapper(TestSuite suite, TestResult result) {
        
        Logger logger = Logger.getLogger(TestResultWrapper.class.getName());
        logger.info("TestResultWrapper::TestResultWrapper() adding result:"+ suite.getName());
        
        name = suite.getName();
        
        errorCount = result.errorCount();
        failureCount = result.failureCount();
        ok = result.wasSuccessful();
        
        Enumeration<Test> e = suite.tests();
        while (e.hasMoreElements()){
            Test test = e.nextElement();      
            results.add( new TestWrapper(test));
        }
        
        if (result.failures() != null) {
            Enumeration<TestFailure> en = result.failures();
            while (en.hasMoreElements()) {
                TestFailure tf = en.nextElement();
                addFailure(tf);
                
            }
        }
        
        if (result.errors() != null) {
            Enumeration<TestFailure> en = result.errors();
            while (en.hasMoreElements()) {
                TestFailure tf = en.nextElement();
                addFailure(tf);
            }
        }
        
        
        
    }

    protected void addFailure(TestFailure tf){
        if (tf.thrownException()!=null){
            tf.thrownException().printStackTrace(System.out);
        }
        Logger logger = Logger.getLogger(TestResultWrapper.class.getName());
        logger.info("TestResultWrapper::TestResultWrapper() "+tf.exceptionMessage());    
       /* for (TestWrapper tw: results){
            if (tw.getTest()==tf.failedTest()) tw.setFailure(tf);
        } */
    }
    
    public String getStatus() {
        return ok ? "OK" : "FAIL";
    }

    public String getName() {
        return name;
    }

    public int getErrorCount() {
        return errorCount;
    }

    public int getFailureCount() {
        return failureCount;
    }

    public Iterator<TestWrapper> getResults() {
        return results.iterator();
    }

}
