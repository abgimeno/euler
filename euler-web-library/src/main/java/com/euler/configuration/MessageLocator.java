/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.PropertyResourceBundle;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author agime
 */
public class MessageLocator {

    protected static MessageLocator singleton;
    protected Vector<PropertyResourceBundle> bundles;

    protected MessageLocator(PropertyResourceBundle bundle) {
        bundles = new Vector();
        bundles.add(bundle);
    }

    public static MessageLocator initInstance(PropertyResourceBundle bundle) {
        singleton = new MessageLocator(bundle);
        return singleton;
    }

    public static MessageLocator initInstance(File file) {

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            PropertyResourceBundle bundle = new PropertyResourceBundle(fis);
            return initInstance(bundle);
        } catch (Exception ex) {
            Logger.getLogger(MessageLocator.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(MessageLocator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    public static MessageLocator getInstance() {
        return singleton;
    }

    public void addBundle(PropertyResourceBundle bundle) {
        bundles.add(bundle);

    }

    public void addBundle(File file) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            PropertyResourceBundle bundle = new PropertyResourceBundle(fis);
            addBundle(bundle);
        } catch (Exception ex) {
            Logger.getLogger(MessageLocator.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(MessageLocator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return;
    }

    public static String getString(String key) {
        String response = null;
        Logger.getLogger(MessageLocator.class.getName()).log(Level.INFO, "MessageLocator::getString() we want [{0}]", key);
        for (PropertyResourceBundle bundle : singleton.bundles) {

            try {
                response = bundle.getString(key);
                return response;
            } catch (Exception e) {
                Logger.getLogger(MessageLocator.class.getName()).log(Level.INFO, null, e);
            }

        }
        return "???" + key + "???";
    }
}
