/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.beans;

/**
 *
 * @author abraham
 */
public class MessageParameter {
    
    
    public static enum ptype {STRING, INT, DOUBLE, FILE, DATE, BOOLEAN};
     
    /** typedefinitoin of the parameter */
    protected  ptype type;
    /** name of the parameter */
    protected  String name;
    /** values of the parameter */
    protected  String values[];
    /** default value, will be returned,if no value was sent by the request */
    protected String defValue; 
    
    public  MessageParameter (String name, ptype type){
        
        this.name=name;
        this.type=type;
    }
    
    public MessageParameter (String name, ptype type, String def){
        
        this.name=name;
        this.type=type;
        this.values = new String[1];
        values[0] = def;
    }    
}
