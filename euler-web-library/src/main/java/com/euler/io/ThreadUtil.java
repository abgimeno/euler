/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.io;

/**
 *
 * @author root
 */
public class ThreadUtil {

    public static String getClassName() {
        final StackTraceElement[] ste = Thread.currentThread().getStackTrace();
        return ste[1].getClassName();
    }

    public static String getMethodName() {
        final StackTraceElement[] ste = Thread.currentThread().getStackTrace();
        return ste[2].getClassName()+":: "+ste[2].getMethodName();   
    }
    /**
     * Get the method name for a depth in call stack. <br /> Utility function
     *
     * @param depth depth in the call stack (0 means current method, 1 means
     * call method, ...)
     * @return method name
     */
    public static String getMethodName(final int depth) {
        final StackTraceElement[] ste = Thread.currentThread().getStackTrace();

        //System. out.println(ste[ste.length-depth].getClassName()+"#"+ste[ste.length-depth].getMethodName());
        // return ste[ste.length - depth].getMethodName();  //Wrong, fails for depth = 0
        return ":: "+ ste[ste.length - 1 - depth].getMethodName(); //Thank you Tom Tresansky
    }       
}
