/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.io;

import com.euler.info.EmailMessageInfo;
import java.io.File;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

/** Send a mail using java mail api
 *
 * @author abraham
 */
public class SendMail {

    public static String MAILHOST = "mail.smtps.host";
    public static String MAILFROM = "mail.from";
    public static String MAILPASS = "mail.password";

    public static void sendMail(EmailMessageInfo emailInfo, Properties props) {
        try {
            Authenticator auth = new SimpleAuthenticator();

            Session session = Session.getInstance(props, auth);
            MimeMessage msg = new MimeMessage(session);

            InternetAddress afrom = new InternetAddress(emailInfo.getFrom());

            InternetAddress ato = new InternetAddress(emailInfo.getTo());

            msg.setFrom(afrom);
            msg.addRecipient(Message.RecipientType.TO, ato);

            if(!emailInfo.getCc().isEmpty()){
                List<InternetAddress> cc = new ArrayList<InternetAddress>();
                for(String s:emailInfo.getCc()){
                    InternetAddress acc = new InternetAddress(s);
                    cc.add(acc);
                }
            }
            if(!emailInfo.getBcc().isEmpty()){
                List<InternetAddress> bcc = new ArrayList<InternetAddress>();
                for(String s:emailInfo.getBcc()){
                    InternetAddress a = new InternetAddress(s);
                    bcc.add(a);
                }
               msg.setRecipients(Message.RecipientType.CC,(InternetAddress []) bcc.toArray());

            }
            
            msg.setSubject(MimeUtility.encodeText(emailInfo.getSubject(), "UTF-8", null));


            Multipart mp = new MimeMultipart();

            // TEXT PART
            BodyPart textPart = new MimeBodyPart();
            textPart.setText(emailInfo.getBody()); // sets type to "text/plain"

            //HTML PART
            BodyPart pixPart = new MimeBodyPart();

            //pixPart.setContent(MimeUtility.encodeText(emailInfo.getBody(), "UTF-8", null), "text/html; charset=utf-8");
            pixPart.setContent(emailInfo.getBody(), "text/html; charset=utf-8");


            // Collect the Parts into the MultiPart
            //mp.addBodyPart(textPart);
            mp.addBodyPart(pixPart);

            msg.setContent(mp);

            Transport transport = session.getTransport();
            transport.connect(props.getProperty(MAILHOST), props.getProperty(MAILFROM), props.getProperty(MAILPASS));
            transport.sendMessage(msg, msg.getAllRecipients());

            Logger.getLogger(SendMail.class.getName()).log(Level.INFO,"SendMail::SendMail() mail sent");

        } catch (Exception ex) {
            Logger.getLogger(SendMail.class.getName()).log(Level.SEVERE,"SendMail::SendMail() exception", ex);
        }
    }

    public static void sendMail(EmailMessageInfo emailInfo, Properties props, File file) {
        try {
            Authenticator auth = new SimpleAuthenticator();

            Session session = Session.getInstance(props, auth);
            MimeMessage msg = new MimeMessage(session);

            InternetAddress afrom = new InternetAddress(emailInfo.getFrom());

            InternetAddress ato = new InternetAddress(emailInfo.getTo());

            msg.setFrom(afrom);
            msg.addRecipient(Message.RecipientType.TO, ato);
            msg.setSubject(MimeUtility.encodeText(emailInfo.getSubject(), "UTF-8", null));


            if(!emailInfo.getCc().isEmpty()){
                InternetAddress[] cc = new InternetAddress[emailInfo.getCc().size()];
                int i=0;
                for(String s:emailInfo.getCc()){
                    InternetAddress acc = new InternetAddress(s);
                    cc[i]=acc;
                    i++;
                }
               msg.setRecipients(Message.RecipientType.CC,cc);
            }
            if(!emailInfo.getBcc().isEmpty()){
                InternetAddress[] bcc = new InternetAddress[emailInfo.getBcc().size()];
                int i = 0;
                for(String s:emailInfo.getBcc()){
                    InternetAddress a = new InternetAddress(s);
                    bcc[i] = a;
                    i++;

                }
               msg.setRecipients(Message.RecipientType.BCC,bcc);
            }

            

            Multipart mp = new MimeMultipart();
            // Create a "related" Multipart message
            BodyPart textPart = new MimeBodyPart();
            textPart.setText(MimeUtility.encodeText(emailInfo.getBody(), "UTF-8", null)); // sets type to "text/plain"

            textPart.setHeader("Content-Type", "text/plain;charset=utf-8");

            BodyPart pixPart = new MimeBodyPart();
            pixPart.setContent(MimeUtility.encodeText(emailInfo.getBody(), "UTF-8", null), "text/html;charset=utf-8");
            pixPart.setHeader("Content-Type", "text/html;charset=utf-8");

            MimeBodyPart mbp = new MimeBodyPart();
            FileDataSource fds = new FileDataSource(file);

            mbp.setDataHandler(new DataHandler(fds));
            mbp.setFileName(fds.getName());

            // Collect the Parts into the MultiPart
            //mp.addBodyPart(textPart);
            mp.addBodyPart(pixPart);
            mp.addBodyPart(mbp);

            msg.setContent(mp);

            Transport transport = session.getTransport();
            transport.connect(props.getProperty(MAILHOST), props.getProperty(MAILFROM), props.getProperty(MAILPASS));
            transport.sendMessage(msg, msg.getAllRecipients());


        } catch (Exception ex) {

            Logger.getLogger(SendMail.class.getName()).log(Level.SEVERE,"SendMail::SendMail() exception",ex);

        }
    }
    public static void sendMail(EmailMessageInfo emailInfo, Properties props, List<File> files) {
        try {
            Authenticator auth = new SimpleAuthenticator();

            Session session = Session.getInstance(props, auth);
            MimeMessage msg = new MimeMessage(session);

            InternetAddress afrom = new InternetAddress(emailInfo.getFrom());

            InternetAddress ato = new InternetAddress(emailInfo.getTo());

            msg.setFrom(afrom);
            msg.addRecipient(Message.RecipientType.TO, ato);
            msg.setSubject(MimeUtility.encodeText(emailInfo.getSubject(), "UTF-8", null));

            if(!emailInfo.getCc().isEmpty()){
                InternetAddress[] cc = new InternetAddress[emailInfo.getCc().size()];
                int i=0;
                for(String s:emailInfo.getCc()){
                    InternetAddress acc = new InternetAddress(s);
                    cc[i]=acc;
                    i++;
                }
               msg.setRecipients(Message.RecipientType.CC,cc);             
            }
            if(!emailInfo.getBcc().isEmpty()){
                InternetAddress[] bcc = new InternetAddress[emailInfo.getBcc().size()];
                int i = 0;
                for(String s:emailInfo.getBcc()){
                    InternetAddress a = new InternetAddress(s);
                    bcc[i] = a;
                    i++;
                    
                }
               msg.setRecipients(Message.RecipientType.BCC,bcc);
            }
            
            Multipart mp = new MimeMultipart();
            // Create a "related" Multipart message
            BodyPart textPart = new MimeBodyPart();
            textPart.setText(MimeUtility.encodeText(emailInfo.getBody(), "UTF-8", null)); // sets type to "text/plain"

            textPart.setHeader("Content-Type", "text/plain;charset=utf-8");

            BodyPart pixPart = new MimeBodyPart();
            pixPart.setContent(MimeUtility.encodeText(emailInfo.getBody(), "UTF-8", null), "text/html;charset=utf-8");
            pixPart.setHeader("Content-Type", "text/html;charset=utf-8");
            
            for (File file:files){
                MimeBodyPart mbp = new MimeBodyPart();
                FileDataSource fds = new FileDataSource(file);
                mbp.setDataHandler(new DataHandler(fds));
                mbp.setFileName(fds.getName());
            // Collect the Parts into the MultiPart
            //mp.addBodyPart(textPart);

                mp.addBodyPart(mbp);

               
            }
            mp.addBodyPart(pixPart);
            msg.setContent(mp);
            Transport transport = session.getTransport();
            transport.connect(props.getProperty(MAILHOST), props.getProperty(MAILFROM), props.getProperty(MAILPASS));
            transport.sendMessage(msg, msg.getAllRecipients());


        } catch (Exception ex) {

            Logger.getLogger(SendMail.class.getName()).log(Level.SEVERE,"SendMail::SendMail() exception",ex);

        }
    }
}
