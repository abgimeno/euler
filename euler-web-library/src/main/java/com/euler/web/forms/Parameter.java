/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.web.forms;

import com.euler.io.ThreadUtil;
import com.euler.web.form.condition.Condition;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.fileupload.FileItem;

/**
 * Class to map all needed Parameters from an incomming request. For every
 * Parameter Conditions can be added. Incoming Parametervalues will be checked
 * with this Conditions (Null,Min- /MaxLength)
 * 
 * @author abraham
 * 
 */
public class Parameter implements Serializable{


    public static enum ptype {STRING, INT, DOUBLE, FILE, DATE, BOOLEAN};
    /** typedefinitoin of the parameter */
    protected  ptype type;
    /** name of the parameter */
    protected  String name;
    /** values of the parameter */
    protected  String values[];
    /** default value, will be returned,if no value was sent by the request */
    protected String defValue; 
    /** ref on a fileitem for uploads */
    protected FileItem fileItem;
    /** Calendar for parsing dates */
    protected SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
    /** if we using tabs, the parameter this is the number of the tab where
     * the parameter is inside
     */
    protected int tab;
    
    /** conditions for the parameter that will be checked */
    protected ArrayList<Condition> conditions=new ArrayList();
    
    public  Parameter (String name, ptype type)
    {
        this.name=name;
        this.type=type;
    }
    
    public Parameter (String name, ptype type, String def)
            {
        this.name=name;
        this.type=type;
        this.values = new String[1];
        values[0] = def;
    }

    public void addCondition(Condition c) {
        conditions.add(c);
    }

    public String getName() {
        return name;
    }

    public Integer getIntegerValue() {
        return Integer.parseInt(getStringValue().trim());
    }

    public String getStringValue() {
        if (values == null || values[0].equals("")) {
            return defValue;
        }
        return values[0];
    }
    
    public boolean getBooleanValue() {
        return values!=null && values[0]!=null;
    }
    
    public void setValue(String s) {
        values = new String[1];
        values[0]=s;
    }
    
    public void setBooleanValue(boolean b) {
        if (b) {
            setValue("true");
        }
        else {
            values=null;
        }
    }
    
    public String [] getStringValues(){
        return values;
    }
    
    public Integer[] getIntegerValues() {
      try{  
        if (values==null) {
            return null;
        }
        Integer[] v= new Integer[values.length];
        int i=0;
        for (String s:values){
            v[i++]= Integer.parseInt(s);
        }
        return v;
      }catch(NumberFormatException nfe){
          Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "{0} there are no integer values in "
                  + "this parameter. Returning an empty array.\n", ThreadUtil.getMethodName());
          return new Integer[0];
      }
    }
    
    public void setDefaultValue(String value){
        defValue=value;
    }
    
    public void setDefaultValue(Date date){
        if (date==null)
        {
            return;
        }
        defValue= sdf.format(date);
    }

    public void map(String v[]) {
        if (v!=null) {
            values = v;
        }
        if (type==ptype.BOOLEAN && (v==null || v[0]==null)) {
            values=null;
        }
    }
    
    public void setFileItem(FileItem item){
        fileItem = item;
    }
    
    public FileItem getFileItem(){
        return fileItem;
    }
    
    public Date getDateValue(){
        try {
            String d = getStringValue();
            if (d==null) {
                return null;
            }
            return sdf.parse(d);
        } catch (ParseException ex) {
            Logger.getLogger(Parameter.class.getName()).log(Level.SEVERE,"Parameter::getDateValue() Exception", ex);
            return null;
        }
    }

    public boolean validate(ArrayList<String> errorlist) {

        try {
            // TYPEN CHECK
            if (type == ptype.DATE && getStringValue() != null) {
                sdf.parse(getStringValue());
            }
            return validateConditions(errorlist);
        } catch (ParseException ex) {
            Logger.getLogger(Parameter.class.getName()).log(Level.INFO, "Parameter::validate() DateFormat is wrong: {0}", ex);
            errorlist.add(name + "_format");
            return false;
        }

    }
    
    protected boolean validateConditions(ArrayList<String> errorlist) {
        
        boolean result =true;        
        for (Condition c : conditions) {
            if (!c.test(values == null ? null : values[0])) {
                result = false;                
                errorlist.add(name + "_" + c.getName());
            }
        }
        return result;
    }
    

    public int getTab() {
        return tab;
    }

    public void setTab(int tab) {
        this.tab = tab;
    }
    
    
}
