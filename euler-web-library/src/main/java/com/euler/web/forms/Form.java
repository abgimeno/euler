/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.web.forms;

import com.euler.io.ThreadUtil;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.FileItem;

/**
 *
 * @author abraham
 */
public class Form {

  
    /** defaul logger, will be overriden by onyx form/s */
    protected static final Logger logger = Logger.getLogger(Form.class.getName());        
    
    protected Map<Integer, Boolean> taberror = new HashMap(); /** keeps track if there are errors in tabs */
    protected Map<String,String> data = new HashMap();   
    protected Map<String, Parameter> params = new HashMap(); /** hashtable with all required fields */
        
    
    
    public void map(HttpServletRequest request, HttpServletResponse response) {

        Enumeration<String> e = request.getParameterNames();
        while (e.hasMoreElements()) {
            String name = e.nextElement();
            String d = request.getParameter(name);
            data.put(name,d);
        }
        
        for (Parameter p : params.values()) {
            String v[] = request.getParameterValues(p.getName());
            logger.log(Level.INFO, ThreadUtil.getMethodName()+"  map parameter [{0}] -> [{1}]", new Object[]{p.getName(), v == null ? "null" : v[0]});
            p.map(v);
        }
    }

    public void mapMultiPart(HttpServletRequest request, HttpServletResponse response) {

        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        logger.log(Level.INFO, ThreadUtil.getMethodName()+"  multipart form is:{0}", isMultipart);
        
        Map<String, Object> mpfd = new HashMap();
        try {
            // Create a new file upload handler and extract items                                      
            List<FileItem> items = createServletFileUpload().parseRequest(request);
            processItems(items, mpfd);
        } catch (FileUploadException ex) {
            logger.log(Level.SEVERE,ThreadUtil.getMethodName()+"  Error handling MultiPartFormData", ex);
        }
        processParameters(params.values(),mpfd);

    }

    public void validate(ArrayList<String> errorlist) {
        for (Parameter p : params.values()) {

            logger.log(Level.INFO, ThreadUtil.getMethodName()+"  check for parameter [{0}]", p.getName());
            if (!p.validate(errorlist)) {
                //we got an error mark tab 
                taberror.put(p.getTab(), false);

            }

        }

    }

    protected void addParameter(Parameter p) {
        params.put(p.getName(), p);
    }

    protected void addParameter(Parameter p, int tab) {
        params.put(p.getName(), p);
        p.setTab(tab);
    }

    protected String getStringValue(String name) {
        return params.get(name).getStringValue();
    }

    protected Integer getIntegerValue(String name) {
        return params.get(name).getIntegerValue();
    }

    public Map<Integer, Boolean> getTaberror() {
        return taberror;
    }

    public boolean getErrorInTab1() {
        boolean ok = taberror.containsKey(1);
        return ok;
    }

    public boolean getErrorInTab2() {
        return taberror.containsKey(2);
    }

    public boolean getErrorInTab3() {
        return taberror.containsKey(3);
    }

    public boolean getErrorInTab4() {
        return taberror.containsKey(4);
    }

    public boolean getErrorInTab5() {
        return taberror.containsKey(5);
    }

    public boolean getErrorInTab6() {
        return taberror.containsKey(6);
    }

    public boolean getErrorInTab7() {
        return taberror.containsKey(7);
    }

    public Map<String, String> getData() {
        return data;
    }

    protected ServletFileUpload createServletFileUpload() {
        // Create a factory for disk-based file items
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(20 * 1024 * 1024);
        return new ServletFileUpload(factory);
    }

    protected void processItems(List<FileItem> items, Map<String, Object> mpfd) {
            
            for (FileItem item : items) {                
                String datavalue = setCorrectStringFormat(item);
                String name = item.getFieldName();
                // If item is a simple form field then value is put on data map
                if (item.isFormField()) {                    
                    data.put(name, datavalue); 
                    // do we have more then 1 value for the item?
                    handleMultipleValuePerItem(name, datavalue,mpfd);

                } else { // Item  represents an uploaded file
                    Parameter fileparam = params.get(name);
                    if (fileparam == null) {
                        logger.log(Level.INFO, ThreadUtil.getMethodName()+"  we expected to find an uploaded file type on Parameter [{0}] but the object returned is null", name);
                    } else {
                        fileparam.setFileItem(item);
                    }
                }
            }
    }

    protected String setCorrectStringFormat(FileItem item) {
        try {
            byte[] bytes = item.getString().getBytes("ISO-8859-1");
            return new String(bytes, "UTF-8");
        } catch (Exception e) {
            logger.log(Level.SEVERE, ThreadUtil.getMethodName(), e);
            return item.getString();
        }
    }

    protected void handleMultipleValuePerItem(String name, String datavalue, Map<String, Object> mpfd) {

        if (mpfd.containsKey(name)) {
            Object multipleItemData = mpfd.get(name);
            if (multipleItemData instanceof String) {
                List<String> v = new ArrayList();
                v.add((String) multipleItemData);
                v.add(datavalue);
                mpfd.put(name, v);
            } else {
                List<String> v = (List) multipleItemData;
                v.add(datavalue);
                mpfd.put(name, v);
            }

            logger.log(Level.CONFIG, ThreadUtil.getMethodName()+" add [{0}] with value [{1}]", new Object[]{name, datavalue});


        } else {
            mpfd.put(name, datavalue);
            logger.log(Level.CONFIG, ThreadUtil.getMethodName()+" ) putting [{0}] with value [{1}]", new Object[]{name, datavalue});
        }
    }

    protected void processParameters(Collection<Parameter> values, Map<String, Object> mpfd) {
        for (Parameter p : params.values()) {
            Object parameterData = mpfd.get(p.getName());
            if (parameterData != null) {
                if (parameterData instanceof String) {
                    String x = (String) parameterData;
                    if (x != null) {

                        String v[] = new String[1];
                        v[0] = x;
                        logger.log(Level.CONFIG, ThreadUtil.getMethodName()+"  map parameter [{0}] -> [{1}]", new Object[]{p.getName(), v == null ? "null" : v[0]});
                        p.map(v);
                    } else {
                        logger.log(Level.INFO, ThreadUtil.getMethodName()+"  parameter [{0}] is null", p.getName());
                    }

                } else {
                    List<String> v = (List) parameterData;
                    String a[] = new String[v.size()];
                    logger.log(Level.INFO, ThreadUtil.getMethodName()+"  set v[] to with {0}elements", v.size());
                    int ctr = 0;
                    for (String x : v) {
                        a[ctr++] = x;
                    }
                    p.map(a);
                }
            } else {
                p.map(null);
            }
        }
    }
    
    
}
