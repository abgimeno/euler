/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.web.paging;

import java.util.Iterator;

/**
 *
 * @author agime
 */
public class PagingIterator implements Iterator {

    private Iterator iterator;
    private int pageSize;
    private int count = 0;

    public PagingIterator(Iterator i, int pageSize, int page) {
        iterator = i;
        this.pageSize = pageSize;
        for (int ctr=0;ctr<page*pageSize;ctr++){
            i.next();
        }
    }

    public boolean hasNext() {
        return (count < pageSize && iterator.hasNext());
    }

    public Object next() {
        count++;
        return iterator.next();
    }

    public void remove() {
        iterator.remove();
    }
}
