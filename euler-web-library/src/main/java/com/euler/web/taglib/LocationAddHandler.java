/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.web.taglib;

import com.euler.web.Link;
import java.io.IOException;
import java.util.List;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author agime
 */
public class LocationAddHandler  extends SimpleTagSupport{
   
    private String url;
    
    private String name;
    
    @Override 
    public void doTag() throws JspException, IOException {
       PageContext context = (PageContext)getJspContext();
       List<Link> list = (List<Link>) context.getSession().getAttribute("position");
       
       Link add = new Link(name,url);
       list.add(add);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
    
}
