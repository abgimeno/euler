/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.web.taglib;

import com.euler.web.Link;
import java.io.IOException;
import java.util.List;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author agime
 */
public class PositionHandler  extends SimpleTagSupport{
   
    
    
    @Override 
    public void doTag() throws JspException, IOException {
       PageContext context = (PageContext)getJspContext();
       List<Link> list = (List<Link>) context.getSession().getAttribute("position");
       
       JspWriter out = this.getJspContext().getOut();
       out.write("<ul>");
       for (Link link : list) {
           out.write("<li><a href=\""+link.getUrl()+"\">"+link.getName()+"</a></li>");
       }
       out.write("</ul>");
           
   }
}
