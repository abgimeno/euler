/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.web.taglib;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author abraham
 */
public class CssContent extends SimpleTagSupport {

    /** class for the div tags */
    private String className;
    /** id for the div tag */
    private String id;

    @Override
    public void doTag() throws JspException, IOException {

        JspWriter out = this.getJspContext().getOut();

        openDiv("top"); closeDiv(); // corner top
        openDiv("content");
        getJspBody().invoke(null);
        closeDiv();
        openDiv("bottom"); // corner - bottom 
        closeDiv();
        
    }

    protected void openDiv(String subName) throws JspException, IOException {
        JspWriter out = this.getJspContext().getOut();
        out.write("<div");

        if (id != null) {
            out.write(" id=\"" + id + "-" + subName + "\"");
        }
        if (className != null) {
            out.write(" class=\"" + className + "-" + subName + "\"");
        }

        out.write(">");
    }

    protected void closeDiv() throws JspException, IOException {
        JspWriter out = this.getJspContext().getOut();
        out.write("</div>");
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public void setId(String id) {
        this.id = id;
    }
}
