

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.web.taglib;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author abraham
 */
public class AnalyticsInit extends SimpleTagSupport {
    
    private String uacct;
    
    public void setUacct(String s){
        uacct=s;
    }
    
    @Override 
    public void doTag() throws JspException, IOException {
        JspWriter out = this.getJspContext().getOut();
        
        out.write("<script type=\"text/javascript\">");
        out.write("var gaJsHost = ((\"https:\" == document.location.protocol) ? \"https://ssl.\" : \"http://www.\");");
        out.write("document.write(unescape(\"%3Cscript src='\" + gaJsHost + \"google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E\"));");
        out.write("</script>");
    }
}

