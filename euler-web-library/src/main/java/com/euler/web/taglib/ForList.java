/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.web.taglib;

/**
 *
 * @author abraham
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author abraham
 */
public class ForList extends SimpleTagSupport {

    /** iterator with the data */
    private Iterator iterator;

    /** name of the variable where we put the entries */
    private String var;

    /** number of cols we want */
    private int cols = 3;


    public void setItems(List list) {
        this.iterator = list.iterator();
    }

    public void setVar(String var) {
        this.var = var;
    }

    public void setCols(int cols) {
        this.cols = cols;
    }


    @Override
    public void doTag() throws JspException, IOException {

        int row = 1;
        int ctr=0;
        int c = 0 ;
        if (iterator==null) return;
        while (iterator.hasNext()) {
            Object o = iterator.next();
            getJspContext().setAttribute(var, o);
            getJspContext().setAttribute("counter", ctr);
            getJspContext().setAttribute("column", c);
            c++;
            if (c==cols) c=0;

            if (row == 1) {
                getJspContext().setAttribute("class", "odd");
            } else {
                getJspContext().setAttribute("class", "even");
            }
            getJspBody().invoke(null);
            row = -row;
            ctr++;
        }
    }
}

