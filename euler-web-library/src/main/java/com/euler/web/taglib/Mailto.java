/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.web.taglib;


import com.euler.info.EmailMessageInfo;
import java.io.IOException;
import java.io.StringWriter;
import javax.servlet.jsp.JspException;

import javax.servlet.jsp.tagext.SimpleTagSupport;

/** Sending the JSPBody of the tag via email to the address given with
 * the setTo() Function
 *
 * @author abraham
 */
public class Mailto extends SimpleTagSupport{
    
    /** class collecting all the data to send the email */
    private EmailMessageInfo emailMessage = new EmailMessageInfo();
    
    @Override
    public void doTag() throws JspException, IOException {   
        StringWriter stringWriter;
        stringWriter = new StringWriter();
        getJspBody().invoke(stringWriter);
        emailMessage.setBody(stringWriter.toString());
        getJspContext().setAttribute("emailMessage", emailMessage);
        
    }

    public void setFrom(String from) {
        this.emailMessage.setFrom(from);
    }

    public void setSubject(String subject) {
        this.emailMessage.setSubject(subject);
    }
    
    public void setTo(String to){
        this.emailMessage.setTo(to);
    }    
    
}
