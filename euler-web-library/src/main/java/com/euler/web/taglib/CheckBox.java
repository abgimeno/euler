

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.web.taglib;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author abraham
 */
public class CheckBox extends SimpleTagSupport {
    
    private String name;
    
    private String value;
    
    private String htmlclass="field";
    
    private int tabindex=1;
        
    @Override 
    public void doTag() throws JspException, IOException {
        JspWriter out = this.getJspContext().getOut();
        String sel = "";
        if (value!=null && value.equals("true")) sel=" checked=\"checked\"";
        out.write("<input type=\"checkbox\" name=\""+name+"\" id=\""+name+"\" class=\""+htmlclass+"\" value=\"true\" tabindex=\""+tabindex+"\""+sel+">");
    }

    public void setHtmlClass(String htmlclass) {
        this.htmlclass = htmlclass;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTabindex(int tabindex) {
        this.tabindex = tabindex;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
    
    
}

