/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.web;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author abraham
 */

public class SessionInfo implements Serializable{
     
    private String sessionId;
    
    private Date creationTime;
    
    private String email;
    
    private String domain;
    
    public SessionInfo(String sessionId, long creationTime, String email, String domain){
        this.sessionId = sessionId;
        this.creationTime = new Date(creationTime);
        this.email = email;
        this.domain = domain;
    }
    public SessionInfo(String sessionId, Date creationTime, String email, String domain){
        this.sessionId = sessionId;
        this.creationTime = creationTime;
        this.email = email;
        this.domain = domain;
    }

    public SessionInfo() {
        
    }
    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }
    
    public void setCreationTime(long creationTime) {
        this.creationTime = new Date(creationTime);
    }   
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }
   
}
