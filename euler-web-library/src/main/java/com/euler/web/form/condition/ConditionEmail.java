/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.web.form.condition;

import com.sun.org.apache.regexp.internal.RESyntaxException;

/**
 *
 * @author abraham
 */
public class ConditionEmail extends ConditionRegExp {

    private static String EMAIL_REGEXP="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    
    public ConditionEmail() throws RESyntaxException {
        super(EMAIL_REGEXP);
    }

    @Override
    public String getName() {
        return "email";
    }
}
