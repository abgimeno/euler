/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.web.form.condition;

import com.sun.org.apache.regexp.internal.RESyntaxException;
import java.text.NumberFormat;

/**
 *
 * @author abraham
 */
public class ConditionIsNumber extends Condition {

    public static final NumberFormat nf = NumberFormat.getInstance();

    
    public ConditionIsNumber() throws RESyntaxException {
    }

    @Override
    public boolean test(String value) {
        try {
            double d = nf.parse(value).doubleValue();
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    @Override
    public String getName() {
        return "isNumber";
    }
}
