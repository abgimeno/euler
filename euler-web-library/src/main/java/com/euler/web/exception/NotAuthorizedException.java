/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.web.exception;

/**
 *
 * @author abraham
 */
public class  NotAuthorizedException extends Exception {
    
    public   NotAuthorizedException(String user){
        super(user + " has tried to perform an unauthorized operation.");        
    }
}
