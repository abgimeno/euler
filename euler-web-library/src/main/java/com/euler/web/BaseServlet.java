/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.web;

import com.euler.web.handler.BaseHandler;
import com.euler.web.exception.HandlerException;
import com.euler.web.exception.RedirectException;
import com.euler.configuration.Configuration;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * Base HttpServlet for all Servlets in
 *
 * <ul> <li> reading a property list for configurations <li> registering handler
 * and calling them depending on the action that is set <li> forwarding to a
 * main jsp page, with a parameter for the jsp to inculde another page </ul>
 *
 * @author agime
 */
public abstract class BaseServlet extends HttpServlet {

    /**
     * Filename of the main property file, relative from the application dir
     */
    public static final String MAIN_PROPERTIES = "config/main.properties";
    public static final String MAIL_PROPERTIES = "config/mail.properties";
       
    public static final String MAIN_PAGE = "/WEB-INF/docs/main.jsp";
    private String defaultHandler = "";
    
    public static final String PAGE_ERROR = "/WEB-INF/docs/error.jsp";
    public static final String PAGE_NONESUCH ="/WEB-INF/docs/nonesuch.jsp";
    
    
    /**
     * Properties with path entries
     */
    private static Properties props, mailprops;
    /**
     * Entries in the main configuration which have to be set
     */
    public static final String MANDATORY_PROPS[] = {"DOCUMENTROOT",};
    /**
     * list of handlernames and its classes
     */

    protected static Logger logger;
    protected static Logger profiler;
    protected Map<String,Class> handlerList= new HashMap<String, Class>();

    
    /**
     * ************** Overwritten Servlet methods **********
     */
    /**
     * init the servlet. Loads the main configuration file
     *
     * @param config servletengine configurationfile
     * @throws javax.servlet.ServletException
     */
    @Override
    public void init(ServletConfig config) throws ServletException {

        super.init(config);
        profiler = Logger.getLogger("com.euler.profiling");
        logger = Logger.getLogger("com.euler");

        try {
            loadProperties();
            loadMailProperties();
            initConfiguration(props, mailprops);
            initHandler();
            logger.info("BaseServlet::init() Application up and running.");

        } catch (Exception ex) {
            Logger.getLogger(BaseServlet.class.getName()).log(Level.SEVERE, null, ex);
            throw new ServletException("Failed to load main configuration:" + ex);
        }

    }

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        setEncoding(request, response);

        logger.log(Level.INFO, "BaseServlet::processRequest() ===================");
        logger.log(Level.INFO, "BaseServlet::processRequest() got a request for :{0}", request.getRequestURL());
        logger.log(Level.INFO, "BaseServlet::processRequest() ===================");

        String page; //= extractPageFromRequest(request);
        setPosition(request);
        initRequest(request, response); //INIT REQUEST CALL, this is an abstract function to be written on implementing class//     
        BaseHandler handler = instantiateHandler(request);                         
        logger.log(Level.INFO, "BaseServlet::processRequest() getting the handler out of the list");

        try {   
            page = executeHandler(request, response, handler);
         }catch (HandlerException he) {
             handleHandlerException(request, response, he);
            return;
        } catch (RedirectException re) {
            response.sendRedirect(re.getUrl());
            logProfiling(new Date(), request);
            return;
        } catch (Exception e) {
            //We got an error that wasnt catched in the handler, show error page
            logger.log(Level.SEVERE, "BaseServlet::processRequest() EXCEPTION - We got an error that wasnt catched in the handler, show error page", e);

            page = PAGE_ERROR;
            getServletContext().log("BaseServlet::processRequest() forwarding to page:" + page);
            forwardTo(request, response, getMainPage(request, page), page);
            //logProfiling(start, request);
            return;
        }



        page = beforeForward(request, response, page);
        forwardTo(request, response, getMainPage(request, page), page);
        //logProfiling(start,request);
    }

    protected void forwardTo(HttpServletRequest request, HttpServletResponse response, String jsp, String includepage) {

        request.setAttribute("page", includepage);
        logger.log(Level.INFO, "BaseServlet::forwardTo() page={0}", includepage);

        try {
            request.getRequestDispatcher(jsp).forward(request, response);
        } catch (Throwable t) {
            getServletContext().log(t.getMessage());
            try {
                request.setAttribute("page", PAGE_ERROR);
                request.getRequestDispatcher(getMainPage(request, PAGE_ERROR)).forward(request, response);

            } catch (Exception e) {
                logger.log(Level.SEVERE, "BaseServlet::forwardTo() EXCEPTION ", e);

            }
        }
    }

    protected void initConfiguration(Properties props, Properties mailprops) {
        Configuration.initInstance(props);
    }

    /**
     * This function is called right before we forward to the jsp
     */
    protected String beforeForward(HttpServletRequest request, HttpServletResponse response, String page) {
        return page;
    }

    protected void setDefaultHandler(String name) {
        defaultHandler = name;
    }

    protected Link getStartPosition() {
        return new Link("inicio", "");
    }

    public static void addToLocation(HttpServletRequest request, Link link) {
        Vector<Link> location = (Vector<Link>) request.getSession().getAttribute("position");
        location.add(link);
    }

    protected void logProfiling(Date start, HttpServletRequest request) {
        Date now = new Date();
        long ms = now.getTime() - start.getTime();
        profiler.log(Level.INFO, "BaseServlet::logProfiling() [{0}] [{1}] ms", new Object[]{request.getRequestURL(), ms});
    }

    protected Cookie createCookie(HttpServletRequest request) {

        String name = (String) request.getParameter("name");
        if (name == null || name.trim().equals("")) {
            request.setAttribute("noname", "noname");
            request.setAttribute("error", "true");
            return null;
        }

        String value = (String) request.getParameter("value");
        if (value == null || value.trim().equals("")) {
            request.setAttribute("novalue", "novalue");
            request.setAttribute("error", "true");
            return null;
        }

        System.out.println(name);
        System.out.println(value);

        Cookie cookie = null;
        try {
            cookie = new Cookie(name, value);
        } catch (IllegalArgumentException ex) {
            // Probably an illegal name
            ex.printStackTrace();
            request.setAttribute("error", "true");
            request.setAttribute("noname", "badname");
            return null;
        }

        String maxage = request.getParameter("maxage");
        if (maxage != null && !maxage.trim().equals("")) {
            try {
                cookie.setMaxAge(Integer.parseInt(maxage));
            } catch (NumberFormatException nfe) {
                nfe.printStackTrace();
                request.setAttribute("badnumber", "badnumber");
                request.setAttribute("error", "true");
                return null;
            }
        }

        String domain = request.getParameter("domain");
        if (domain != null && !domain.trim().equals("")) {
            cookie.setDomain(domain);
        }

        String path = request.getParameter("path");
        if (path != null && !path.trim().equals("")) {
            cookie.setPath(path);
        }

        String secure = request.getParameter("secure");
        if (secure != null && secure.equals("on")) {
            cookie.setSecure(true);
        } else {
            cookie.setSecure(false);
        }
        return cookie;
    }

    protected void loadProperties() throws Exception {
        File file = null;
        try {
            file = new File(getServletContext().getRealPath(MAIN_PROPERTIES));
            props = new Properties();
            props.load(new FileInputStream(file));
            if (missingProperties()) {
                throw new ServletException("BaseServlet::loadProperties() Properties missing in " + MAIN_PROPERTIES + ". Check log.");
            }
        } catch (IOException ex) {
            logger.log(Level.INFO, "BaseServlet::loadProperties() Failed to load  configuration file at [{0}], aborting", file.toString());
            Logger.getLogger(BaseServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    protected boolean missingProperties() {
        // Check for mandatory entries in props
        int missing = 0;
        boolean result = false;
        for (String propname : getMandatoryPros()) {
            if (props.getProperty(propname) == null) {
                missing++;
                result = true;
                logger.log(Level.INFO, "BaseServlet::missingProperties() Property [{0}" + "] missing in " + MAIN_PROPERTIES, propname);
            }
        }
        return result;
    }

    protected void loadMailProperties() {
        File file = null;
        try {
            file = new File(getServletContext().getRealPath(MAIL_PROPERTIES));
            mailprops = new Properties();
            mailprops.load(new FileInputStream(file));
        } catch (IOException ex) {
            logger.log(Level.INFO, "Failed to load configuration file at [{0}], aborting", file.toString());
            Logger.getLogger(BaseServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected void setEncoding(HttpServletRequest request, HttpServletResponse response) {
        try {
            request.setCharacterEncoding("UTF-8");
            response.setContentType("text/html;charset=utf-8");
        } catch (Exception ce) {
            logger.log(Level.SEVERE, "BaseServlet::setEncoding() encoding setting problem", ce);
        }

    }

    protected String extractPageFromRequest(HttpServletRequest request) {

        // The page variable holds the path to the page that will be included
        // in the content area of the template page /WEB-INF/docs/main.jsp.
        // It is passed to the template in a request attribute.
        String id = request.getRequestURI(), page = null;
        id = id.substring(request.getContextPath().length());


        // If no request is received or if the request received is
        // for the root, serve /WEB-INF/docs/index.jsp
        if (id == null || id.trim().equals("") || id.equals("/")) {
            page = "/WEB-INF/docs/index.jsp";
        } else {
            page = "/WEB-INF".concat(id);
        }
        return page;
    }

    protected void setPosition(HttpServletRequest request) {
        // Position
        Link home = getStartPosition();
        List<Link> list = new Vector();
        list.add(home);
        request.getSession().setAttribute("position", list);
    }
    
    private BaseHandler instantiateHandler(HttpServletRequest request ){
        String handlername = getHandlerType(request);// extracts the handler that from request if any, otherwise returns default handler
        logger.log(Level.INFO, "BaseServlet::instantiateHandler() calling handler [{0}]", handlername);
        return instantiateHandlerClass(getHandler().get(handlername));  // return the handler out of the handlers list  
    }
    
    
    protected String getHandlerType(HttpServletRequest request) {

        String handler = request.getParameter("handler");
        // if there is attribtue handlername set, use this handler !
        if (request.getAttribute("HANDLER") != null) {
            handler = (String) request.getAttribute("HANDLER");
        }

        if (handler == null) {
            handler = defaultHandler;
        }
        return handler;
    }

    protected BaseHandler instantiateHandlerClass(Class c) {
        if (c != null) {
            try {
                return (BaseHandler) c.newInstance();
            } catch (InstantiationException ex) {
                logger.log(Level.SEVERE, "BaseServlet::instantiateHandlerClass() EXCEPTION", ex);
            } catch (IllegalAccessException ex) {
                logger.log(Level.SEVERE, "BaseServlet::instantiateHandlerClass() EXCEPTION", ex);
            }
        }
        return null;
    }

    protected String executeHandler(HttpServletRequest request, HttpServletResponse response, BaseHandler handler) throws HandlerException{
        String page;
        if (handler != null) {
            logger.log(Level.INFO, "BaseServlet::executeHandler() calling handlerclass [{0}]", handler.getClass().getName());
            beforeProcessRequest(handler, request, response);// CALLING FUCTION TO INIT HANDLER BEFORE CALL //
            request.setAttribute("handler", handler);
            handler.processRequest(request, response);
            page = handler.getJSP();
        } else {
            logger.log(Level.INFO, "BaseServlet::executeHandler() Handler [{0}] NOT FOUND",handler.getClass().getName());
            page = PAGE_NONESUCH;
        }
        return page;
    }

    private void handleHandlerException(HttpServletRequest request, HttpServletResponse response, HandlerException he ) {
        logger.log(Level.SEVERE, "BaseServlet::handleHandlerException()", he);
        String page = PAGE_ERROR;
        //getServletContext().log("forward to page:" + page);
        request.setAttribute("error", he.getMessage());
        forwardTo(request, response, getMainPage(request, page), page); // CALLING FORWARDTO ///
        logProfiling(new Date(), request);

    }

    protected void addHandler(String name, Class c) {
        handlerList.put(name, c);
    }    
 
    protected Map<String,Class> getHandler() {
        return handlerList;
    }
    
    // Mandatory props are set in eacth particular project that uses this class, normally this function should be overwriten
    protected String[] getMandatoryPros() {
        return MANDATORY_PROPS;
    }

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

    /**
     * ************** End of Overwritten Servlet methods **********
     */


    /**
     * Override to add all known handler classes. Use addHandler() to register
     * handler
     */
    protected abstract void initHandler();

    /**
     * will be called, before the handler->proccessRequest is called
     */
    protected abstract void beforeProcessRequest(BaseHandler handler, HttpServletRequest request, HttpServletResponse response);

    /**
     * will be called at every request, before anything is done
     */
    protected abstract void initRequest(HttpServletRequest request, HttpServletResponse response);

    protected String getMainPage(HttpServletRequest request, String page) {
        return MAIN_PAGE;
    }



    /**
     * ************** End of Abstract methods **********
     */   
}
