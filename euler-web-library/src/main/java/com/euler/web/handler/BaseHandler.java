/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.web.handler;

import com.euler.web.exception.HandlerException;
import com.euler.web.exception.RedirectException;
import com.euler.web.interfaces.Handler;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *
 * @author abraham
 */
public abstract class BaseHandler implements Handler {

    /** defaul logger */

    /** list of errors */
    protected ArrayList<String> errorlist;
    /** button names should starti with this name */
    public static final String SUBMIT = "SUBMIT";
    /** name of the button that was submited */
    protected String submit;
    /** true if the requet was a POST request */
    protected boolean isPost;
    protected boolean isUserInSession;

    public static final String PARAM_ACTION="action";
            
    public static final String ATTRIBUTE_DESCRIPTION="description";
    public static final String ATTRIBUTE_USER="userAccount";
    public static final String ATTRIBUTE_ISUSER="isUser";
    public static final String ATTRIBUTE_TITLE="title";
    
    protected String action;
    protected String page;
    protected String description;
    protected String title;

    HttpServletRequest request;
    
    protected Logger logger;

    
    public BaseHandler() {
        errorlist = new ArrayList();        
    }

    @Override
    public ArrayList<String> getErrorList() {
        return errorlist;
    }

    /** if you want that a different page is shown then the requested, return here the pagename 
     *  start with / !
     * @return
     */
    @Override
    public String getJSP() {
        return "";
    }

    /** handle the request, get all parameters */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws HandlerException {
        
        this.request=request;
        action= request.getParameter(PARAM_ACTION);        
        setPost(); // true if current request is post
        setUserInSession(); // true if user in session
        resetErrorList(); // empties the error list used by validate request
        boolean ok = validateRequest(request, response);       
        setSEOTags();
        if (ok && isPost) {                
                try{
                    Logger.getLogger(BaseHandler.class.getName()).info("Handler::processRequest() validation ok, calling handleRequest");
                    handleRequest(request, response);
                } catch(RedirectException ex){
                    Logger.getLogger(HandlerServlet.class.getName()).log(Level.INFO, "HandlerServlet::processRequest() Redirecting to {0}", ex.getUrl());
                    try {
                        response.sendRedirect(ex.getUrl());
                    } catch (IOException ex1) {
                        java.util.logging.Logger.getLogger(BaseHandler.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                }
        } else {
            for (String es : errorlist) {
                Logger.getLogger(BaseHandler.class.getName()).log(Level.SEVERE, " HandlerServlet::processRequest() Condition failed: [{0}]", es);
            }
        }
    }

    /** let the subclass do all needed check to see if the request is valid */
    protected abstract boolean validateRequest(HttpServletRequest request, HttpServletResponse response) throws HandlerException;

    protected abstract void handleRequest(HttpServletRequest request, HttpServletResponse response) throws HandlerException;

    /****** MODIFIED FOR DYNAMICALLY A DESCRIPTION TO BE USED IN THE META TAG DESCRIPTION**********/
    protected abstract String getDescription();
    protected abstract String getTitle();

    @Override
    public boolean isSubmit(String name) {
        if (request.getParameter(name)!=null) {
            return true;
        }
        if (request.getParameter(name+".x")!=null){
            return true;
        }
        return false;
    }

    /**
     * Get the method name for a depth in call stack. <br /> Utility function
     *
     * @param depth depth in the call stack (0 means current method, 1 means
     * call method, ...)
     * @return method name
     */
    public static String getMethodName(final int depth) {
        final StackTraceElement[] ste = Thread.currentThread().getStackTrace();

        //System. out.println(ste[ste.length-depth].getClassName()+"#"+ste[ste.length-depth].getMethodName());
        // return ste[ste.length - depth].getMethodName();  //Wrong, fails for depth = 0
        return ste[ste.length - 1 - depth].getMethodName(); //Thank you Tom Tresansky
    }    

    private void setUserInSession() {
        /**
         * **** MODIFICATION FOR CHECK IF USER IS IN SESSION ******
         */
        if (request.getSession().getAttribute(ATTRIBUTE_USER) != null) {
            isUserInSession = true;
            request.setAttribute(ATTRIBUTE_ISUSER, isUserInSession);
        } else {
            isUserInSession = false;
            request.setAttribute(ATTRIBUTE_ISUSER, isUserInSession);
        }
    }

    private void setPost() {
        if (request.getMethod().equals("POST")) {
            isPost = true;
        } else {
            isPost = false;
        }
    }

    private void resetErrorList() {
        // reset errorlist
        errorlist.clear();
        request.setAttribute("errorlist", errorlist);
    }

    private void setSEOTags() {
        //setting description for the meta tag
        request.setAttribute(ATTRIBUTE_DESCRIPTION, getDescription());
        request.setAttribute(ATTRIBUTE_TITLE, getTitle());
    }
}
