/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.web.handler;



import com.euler.web.handler.BaseHandler;
import com.euler.web.exception.HandlerException;
import com.euler.web.exception.RedirectException;
import com.euler.configuration.Configuration;
import com.euler.jpa.controllers.UserJpaController;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author abraham
 */
public abstract class HandlerServlet extends HttpServlet {

    public static final String MAIL_PROPERTIES = "/WEB-INF/config/mail.properties";

    public static final String PAGE_ERROR="/WEB_INF/docs/error.jsp";
    public static final String PAGE_NOTFOUND = "/WEB-INF/docs/nonesuch.jsp";
    public static       String MAIN_PAGE = "/WEB-INF/docs/main.jsp";

    public static final String PARAM_HANDLER = "serv";
    public static final String ATTRB_HANDLER = "HANDLER";
    public static final String ATTRB_DESCRIPTION="description";
    public static final String ATTRB_MAIN_PAGE="main_page";
   
    public static UserJpaController user2Ctl=null;

    public static String persistenceUnitName="standard-PU";
    
    private static Properties mailprops;
    protected String defaultHandler;
    protected String page;
    protected Hashtable<String, Class> handlers = new Hashtable();
    private File file;


    protected void addHandler(String name, Class c) {
        handlers.put(name, c);
    }

    protected Hashtable<String, Class> getHandlers() {
        return handlers;
    }

    protected String getMainPage(HttpServletRequest request, String page) {
        if(request.getAttribute("main_page") != null){
            MAIN_PAGE=(String)request.getAttribute("main_page");
            }
        return MAIN_PAGE;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        try {
            super.init(config);
            file = new File(getServletContext().getRealPath(MAIL_PROPERTIES));
            mailprops = new Properties();
            mailprops.load(new FileInputStream(file));
            Configuration.initInstance(mailprops);
            initHandler();
            //initJpaControllers();
        }catch (FileNotFoundException ex) {
            Logger.getLogger(HandlerServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(HandlerServlet.class.getName()).log(Level.SEVERE, null, ex);
        } 

    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // handle the request with a requesthandler
        String handlername = request.getParameter(PARAM_HANDLER);
        Logger.getLogger(HandlerServlet.class.getName()).info("STARTING SERVLET TRANSACTION");
        Logger.getLogger(HandlerServlet.class.getName()).info("calling: " + request.getRequestURL().toString().trim()+"?serv="+handlername+"&action="+request.getParameter("action"));
        Logger.getLogger(HandlerServlet.class.getName()).info("calling: " + request.getRemoteHost());
        // if there is attribtue handlername set, use this handler !
        if (request.getAttribute(ATTRB_HANDLER) != null) {
            handlername = (String) request.getAttribute(ATTRB_HANDLER);
        }
        if (handlername == null) {
            handlername = defaultHandler;
        }

        if (handlername != null) {
            //log.info("BaseServlet::processRequest() calling handler [" + handlername + "]");                
            // get the handler out of the list
            Class c = getHandlers().get(handlername);
            if (c != null) {
                try {
                    BaseHandler handler = (BaseHandler) c.newInstance();
                    Logger.getLogger(HandlerServlet.class.getName()).info("HandlerServlet::processRequest() calling handlerclass [" + c.toString() + "]");
                    // CALL FUCTION TO INIT HANDLER BEFORE CALL
                    //beforeProcessRequest(handler, request, response);
                    request.setAttribute(PARAM_HANDLER, handler);
                    request.setAttribute(ATTRB_DESCRIPTION, handler.getDescription()); //sets the page description
                    handler.processRequest(request, response);                    
                    if (handler.getJSP() != null) {
                        page = handler.getJSP();
                    }

                forwardTo(request, response,getMainPage(request, page), page);
                } catch (InstantiationException ex) {
                    Logger.getLogger(HandlerServlet.class.getName()).log(Level.SEVERE,"HandlerServlet::processRequest()", ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(HandlerServlet.class.getName()).log(Level.SEVERE,"HandlerServlet::processRequest()", ex);
                } catch (HandlerException ex) {
                    Logger.getLogger(HandlerServlet.class.getName()).log(Level.SEVERE,"HandlerServlet::processRequest()", ex);
                }catch (RedirectException re) {
                    forwardTo(request, response,getMainPage(request, re.getUrl()), re.getUrl());
               }
            } else {
                Logger.getLogger(HandlerServlet.class.getName()).log(Level.SEVERE,"HandlerServlet::processRequest() Page not found");
                page = PAGE_NOTFOUND;
                forwardTo(request, response,getMainPage(request, page), page);
            }

        }


    }
    protected void forwardTo(HttpServletRequest request, HttpServletResponse response, String jsp, String includepage) {
        
        request.setAttribute("page", includepage);

        try {
            request.getRequestDispatcher(jsp).forward(request, response);
        } catch (Throwable t) {
            getServletContext().log(t.getMessage());

            try {
                request.setAttribute("page", PAGE_ERROR);
                request.getRequestDispatcher(PAGE_ERROR).forward(request, response);

            } catch (Exception e) {
                Logger.getLogger(HandlerServlet.class.getName()).log(Level.SEVERE,"HandlerServlet::forwardTo()", e);

            }
        }
    }
    protected abstract void initHandler();
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    protected abstract void initJpaControllers();
}
