/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.info;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.TableGenerator;

/**
 *
 * @author abrahamgimeno
 */
@Embeddable
public class User implements Serializable {

    public static final String DEF_PWD = "JACK99";
    public static final String DEF_ROOT = "admin@euler.com";

    private static final long serialVersionUID = 1L;

    @TableGenerator(name = "userGen", table = "SEQUENCE_TABLE",
            pkColumnName = "GEN_KEY", valueColumnName = "GEN_VALUE",
            pkColumnValue = "PK_USER_ID", allocationSize = 1, initialValue = 1)
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "userGen")
    private Long id;

    @Column(name = "ACCOUNT_EMAIL")
    private String emailAccount;

    @Column(name = "DNI")
    private String dni;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "ENABLED_ACCOUNT")
    private boolean enabled;

    @Embedded
    private Person personalInfo;

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getEmailAccount() {
        return emailAccount;
    }

    public void setEmailAccount(String emailAccount) {
        this.emailAccount = emailAccount;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Person getPersonalInfo() {
        return personalInfo;
    }

    public void setPersonalInfo(Person personalInfo) {
        this.personalInfo = personalInfo;
    }

}
