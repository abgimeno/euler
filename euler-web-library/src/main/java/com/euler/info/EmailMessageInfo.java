/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.info;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author abraham
 * 
 */
@Embeddable
public class EmailMessageInfo implements Serializable{
                  
    @Column(name="BODY")    
    private String body;
    
    @Column(name="FROM_ADDRESS")
    private String fromAddress;    
    
    @Column(name="TO_ADDRESS")
    private String toAddress;    
    
    @Column(name="SUBJECT")
    private String subject;   

    private List<String> cc = new ArrayList<String>();

    private List<String> bcc = new ArrayList<String>();
    
    public EmailMessageInfo  () {
        
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getFrom() {
        return fromAddress;
    }

    public void setFrom(String from) {
        this.fromAddress = from;
    }

    public String getTo() {
        return toAddress;
    }

    public void setTo(String to) {
        this.toAddress = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<String> getBcc() {
        return bcc;
    }

    public void setBcc(List<String> bcc) {
        this.bcc = bcc;
    }

    public List<String> getCc() {
        return cc;
    }

    public void setCc(List<String> cc) {
        this.cc = cc;
    }

    
}
