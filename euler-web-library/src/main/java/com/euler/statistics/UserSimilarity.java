/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.statistics;

/**
 *
 * @author abraham
 * 
 */
public class UserSimilarity extends Similarity{
    
    
    private int userId;
    private int critic;
    private double similarity;
    
    public UserSimilarity(){
        
    }

    public UserSimilarity(int userId, int critic, double similarity) {
        this.userId = userId;
        this.critic = critic;
        this.similarity = similarity;
    }

    public void setSimilarity(double similarity) {
        this.similarity = similarity;
    }
    
    
    @Override
    public double getSimilarity() {
        return similarity;
    }

    public int getCritic() {
        return critic;
    }

    public void setCritic(int critic) {
        this.critic = critic;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
    
    

}
