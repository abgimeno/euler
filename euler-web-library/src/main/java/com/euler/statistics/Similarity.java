/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.statistics;

/**
 *
 *@author abraham
 * 
 */
public abstract class Similarity {
    
    public abstract double getSimilarity();
    
}
