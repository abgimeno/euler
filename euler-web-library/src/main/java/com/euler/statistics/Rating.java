/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.statistics;

/**
 *
 * @author abraham
 */
public abstract class Rating {
    
    public abstract int getProductId();
    public abstract int getUserId();
    public abstract double getRate();
}
