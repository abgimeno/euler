/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.statistics;

import java.util.Comparator;

/**
 *
 * @author abraham
 */
public class UserSimilComp implements Comparator{

    public int compare(Object o1, Object o2) {
        
        UserSimilarity user1 = (UserSimilarity) o1;
        UserSimilarity user2 = (UserSimilarity) o2;
        
        if(user1.getSimilarity() == user2.getSimilarity())
            return 0;
        if (user1.getSimilarity() > user2.getSimilarity())
            return 1;
        else 
            return -1;
    }

}
