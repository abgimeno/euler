/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.statistics;

import java.io.Serializable;
/**
 *
 * @author abraham
 */
public class RatingCK implements Serializable{
  
    public RatingCK (){
        
    }
    
    protected int userId;
        
    protected int productId;
    
    @Override
    public boolean equals(Object other) {
        if(other instanceof RatingCK){
             RatingCK otherReview = (RatingCK) other;
             return (otherReview.productId == this.productId && otherReview.userId==this.userId);
        }   
        
        return false;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getUser() {
        return userId;
    }

    public void setUser(int userId) {
        this.userId = userId;
    }

}
