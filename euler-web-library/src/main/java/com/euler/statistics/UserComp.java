/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.statistics;

import java.util.Comparator;

/**
 *
 * @author abraham
 * 
 * Compare 2 users with the target of sort a list of users
 */
public class UserComp implements Comparator {

    public int compare(Object o1, Object o2) {
        Rating r1 = (Rating) o1;
        Rating r2 = (Rating) o2;
        
        if(r1.getUserId() == r1.getUserId())
            return 0;
        if(r1.getUserId() > r1.getUserId())
            return 1;
        else 
            return -1;
    }

}
